### **ELASTIC COMPUTING USANDO Redis**

O pacote `doRedis` usa a ideia de uma fila de trabalho (`work queue`) para dividir as atividades dentre os recursos disponíveis. Cada job `doRedis` é composto de um conjunto de uma ou mais atividades (`tasks`). Cada task consiste de uma ou mais iterações `foreach`. O R fica aguardando na lista de trabalho por novos `jobs`.

Uma iteração `foreach` é uma expressão R que representa um único trabalho. A expressão é o corpo do loop `foreach` e o parâmetro, se ele existe, é uma variável do loop. Note que também é possível usar `foreach` não paramétricamente com um iterador anônimo para simplismente replicar a expressão do loop um certo número de vezes. Cada iteração é enumerada de modo que `foreach` possa pegar os resultados juntos e em ordem, se requerido.

Uma `task` é uma coleção de uma ou mais iterações do loop. O número de iterações por task é no máximo `chunkSize`. Um `job` é uma coleção de uma ou mais tasks que abrangem todas as iterações no loop `foreach`. A cada job é atribuido um único identificador.

`Jobs` são submetidos como uma sequência de tasks para uma fila de trabalho (`work queue`). Usuários escolhem o nome da fila de trabalho na função
`registerDoRedis`. O R escuta a fila de trabalho por tasks.

#### **FUNÇÕES PARA CONTROLE DE ATIVIDADES**

O pacote inclui algumas funções convenientes para monitorar e controlar o trabalho de `doRedis`. O mais simples, `setProgress(TRUE)`, adiciona um indicador de progresso dos foreach loops. É uma maneira útil e padrão de monitorar o progresso de atividades do R. 

Uma vez que o R controla blocos do programa enquanto ele espera por tasks `doRedis` serem finalizadas, a maioria das outras formas de monitoramento e controle devem ser executadas em uma sessão diferente do R. Temos as seguintes funções: 

* `jobs()` retorna um data frame que lista todos os jobs em execução e informações sobre eles;
* `tasks()` retorna um data frame de tasks em execução; 
* `removeQueue()` remove chaves Redis associadas com uma lista de trabalho `doRedis`; 
* `removeJob()` remove todas as tasks remanescentes associadas com um job específico na lista de trabalho.

A função `task()` lista o nome da fila do Redis de cada `task` em execução, o id do job associado com ela, o usuário executando o job, a partir do qual o ip foi submetido, o endereço do ip do computador no qual a task está sendo executada, o usuário e o id do processo se o R está executando a task. Note que tasks podem permanecer na lista de tasks por um breve período depois que elas já foram finalizadas. A função `removeQueue()` remove todas as chaves Redis associadas com a lista de trabalho especificada. 


#### **ORGANIZAÇÃO DO Redis**

![keys](linktokeyspage)

O nome `work queue` especificado nas funções `registerDoRedis` e `redisWorker` é usado como o nome raiz para a família de chaves Redis usadas para organizar o trabalho. A figura acima ilustra o exemplo de chaves Redis usadas por um processo R master e worker para uma lista de trabalho chamada de `myjobs`, descritos em detalhes.

O nome da fila de de trabalho na figura é `myjobs`. A correspondente chave Redis é também chamada de `myjobs` e ela é uma lista Redis do tipo valor (ou seja, uma fila). Tal fila pode ser configurada, por exemplo, com `registerDoRedis(queue="myjobs"). 

A remoção da chave `jobs.live` serve como um sinal de que a fila de trabalho foi removida, por exemplo com a função `removeQueue("myjobs")`. Depois que isto acontece, os workers do R escutando a fila irão limpar qualquer chave Redis que eles criaram e terminam depois do período de timeout. 

Uma chave contadora chamada `myjobs.count` enumera o número de workers do R registrados na fila. Ela é apenas uma estimativa do número de workers correntemente registrados para aceitar trabalho de uma fila. 

`foreach` atribui a cada job um ambiente do R com o estado necessário para executar a expressão do loop. O exemplo na figura ilustra um ambiente para a fila `myjobs` e o job `jobID` chamado de `myjobs.env.jobID` sendo que o worker do R trabalhando sobre `jobID` fará o download deste ambiente uma vez (independentemente da quantidade de tasks que está executando para o job).

`doRedis` publica as tasks (iterações do loop) associadas com `jobID` dentro da fila `myjobs` usando o label `jobID.n`, onde `n` é o número de cada task. 

O worker do R escuta a lista `myjobs` por tasks. Depois de cada time out ele verifica se a chave `myjobs.live` ainda existe e se ela não está, ele limpa suas chaves Redis e finaliza. Se ela ainda está lá, ele executa o loop e escuta novamente por jobs. 

Quando um job/task chega na lista `myjobs`, um worker baixa a nova task a partir da lista `myjobs`. O worker então:

* Verifica o ID do job para ver se ele já tem um ambiente. Se ele não tem, ele baixa o ambiente (a partir de `myjobs.env.jobID`) e inicializa um novo ambiente R específico para este ID.
* O worker do R inicializa uma task, ilustrada na figura como `myjobs.start.jobID.taskID`;
* O worker do R inicializa um encadeamento que mantém uma task, ilustrada na figura como `myjob.alive.jobID.taskID`;
* Quando uma task é completada, o worker coloca o resultado em uma fila `job result` para este ID, mostrado na figura como `myjobs.out.jobID` e então remove suas correspondentes chaves de inicialização e que diz que ele continua vivo (alive).

Enquanto isso, o processo master do R está esperando por resultados na lista de `job result`, mostrado na figura como `myjobs.out.jobID`. Resultados chegam por meio dos workers como listas do R da forma `list(id=result)`, onde `id` corresponde ao ID da task e `result` ao resultado da task. `foreach` pode usar o `ID` para armazenar em cache e ordernar os resultados, ao menos que a opção `.inorder=FALSE` foi específicada.


#### **DETECÇÃO DE FALHAS E RESTAURAÇÃO DE UM WORKER**

Durante a execução, cada task está associada com duas chaves descritas anteriormente: um chave de inicialização, `start`, e uma chave que diz que a task continua viva, `alive`. A primeira é criada por um worker do R quando uma task é baixada para inicio de execuação. A segunda é uma chave Redis com um time out relativamente curto. Cada worker do R mantém uma encadeamento (thread) em background que mantêm a segunda chave ativa enquanto a task está em execução. Se por alguma razão o worker do R trava, falha ou o sistema falha ou reinicializa, ou a conexão falha, então a segunda chave irá expirar e será removida do Redis.

Depois que `foreach` configura um job, o processo R master escuta por resultados sobre o ID associado do job da fila de resultados. Depois de cada expiração, o master examina todas as chaves (`start` e `alive`) associadas com o job. Se ele encontra uma delas, mas a outra não, então o processo R master assume que a task falhou e submete novamente a task na fila de trabalho. 


#### **EXEMPLO**

```r
install.packages("doRedis")
require(doRedis)
options('redis:num'=TRUE) # opção para retornar mensagens do Redis como valores numéricos
registerDoRedis("RJOBS") # registra o back end com foreach usando o nome da fila especificado pelo usuário
startLocalWorkers(n=4, queue = "RJOBS") # inicia duas seções do R no computador local. 
getDoParWorkers() # verifica que os workers estão de fato esperando por trabalho 
cat("Workers started.\n")
start = Sys.time()
x = foreach(j=1:4, .combine=sum, .verbose = TRUE, 
.options.redis = list(ftinterval=5, chunkSize=2))%dopar% 
{
if(difftime(Sys.time(),start)<5) quit(save="no")
j
}
removeQueue("RJOBS")
```


A função `ftinterval` define um limite superior de frequência na qual `doRedis` verifica a existência de falhas. O valor default é de 30 segundos e o mínimo permitido é de 3 segundos.

