function [sigma2_i_new]=Generate_sigma2_i(y_i,x_i,p_i,phi_i,beta_i,v_i_bar,delta_i_bar)

% Generates draw for sigma^2_i based on eq. (8) in Otrok and
% Whiteman (1998) which in turn is based on Chib and
% Greenberg (1994, Section 4.1). The notation closely follows
% Otrok and Whiteman (1998).
%
% Calls Generate_Sigma_i
%
% Input:
%
% y_i         = vector of observations for y_i
% x_i         = matrix of RHS variables (previous factor draws) for i
% p_i         = AR order for idiosyncratic shock for i
% phi_i       = previous AR coefficients draw for i
% beta_i      = previous factor loading draw for i
% v_i_bar     = first parameter of inverted gamma prior for sigma^2_i
% delta_i_bar = second parameter of inversted gamma prior for sigma^2_i
%
% Output:
%
% sigma2_i_new = new sigma^2_i draw
%
% References:
%
% C. Otrok and C. Whiteman, "Bayesian Leading Indicators: Measuring and
% Predicting Economics Conditions in Iowa," International Economic Review
% 39, 997-1014
%
% S. Chib and E. Greenberg, "Bayes Inference in Regression Models with ARMA(p,q)
% Errors," Journal of Econometrics 64, 183-206.

T=size(y_i,1); % number of time-series observations
k=size(x_i,2); % number of regressors
y_i_1_tilde=y_i(1:p_i,1); % first p_i observations for y_i
x_i_1_tilde=x_i(1:p_i,:); % first p_i observations for x_i
Sigma_i=Generate_Sigma_i(phi_i);
Q_i_t=chol(Sigma_i); % Sigma_i = Q_i_t'Q_i_t
Q_i_inv=inv(Q_i_t)'; % see OW, p. 1001
y_i_1_tilde_star=Q_i_inv*y_i_1_tilde;
x_i_1_tilde_star=Q_i_inv*x_i_1_tilde;
y_i_2_tilde_star=zeros(T-p_i,1);
x_i_2_tilde_star=zeros(T-p_i,k);
phi_i_reverse=zeros(p_i,1);
for iter=1:p_i;
    phi_i_reverse(iter)=phi_i(p_i-(iter-1));
end;
for iter=1:(T-p_i);
    y_i_2_tilde_star(iter)=[-phi_i_reverse' 1]*y_i(iter:p_i+iter); % see OW, p. 1001
    x_i_2_tilde_star(iter,:)=[-phi_i_reverse' 1]*x_i(iter:p_i+iter,:);
end;
y_i_tilde_star=[y_i_1_tilde_star ; y_i_2_tilde_star]; % see OW, p. 1001
x_i_tilde_star=[x_i_1_tilde_star ; x_i_2_tilde_star];
n_1=v_i_bar+T; % first parameter for inverted gamma posterior in (8)
d_i=(y_i_tilde_star-x_i_tilde_star*beta_i)'*(y_i_tilde_star-x_i_tilde_star*beta_i); % see OW, p. 1002
n_2=delta_i_bar+d_i; % second parameter for inverted gamma posterior in (8)
c=chi2rnd(n_1);
t_2=c/n_2;
sigma2_i_new=1/t_2;
