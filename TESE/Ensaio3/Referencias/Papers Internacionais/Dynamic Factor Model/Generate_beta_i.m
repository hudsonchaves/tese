function [beta_i_new]=Generate_beta_i(y_i,x_i,p_i,phi_i,beta_i_bar,B_i_bar_inv,s2_i)

% Generates draw for beta_i based on eq. (6) in Otrok and
% Whiteman (1998) which in turn is based on Chib and
% Greenberg (1994, Section 4.1). The notation closely follows
% Otrok and Whiteman (1998).
%
% Calls Generate_Sigma_i
%
% Input:
%
% y_i         = vector of observations for y_i
% x_i         = matrix of RHS variables (previous factor draws) for i
% p_i         = AR order for idiosyncratic shock for i
% phi_i       = previous AR coefficients draw for i
% beta_i_bar  = prior mean for factor loadings
% B_i_bar_inv = prior precision for factor loadings
% s2_i        = previous idiosyncratic variance draw for i
%
% Output:
%
% beta_i_new = new beta_i draw
%
% References:
%
% C. Otrok and C. Whiteman, "Bayesian Leading Indicators: Measuring and
% Predicting Economics Conditions in Iowa," International Economic Review
% 39, 997-1014
%
% S. Chib and E. Greenberg, "Bayes Inference in Regression Models with ARMA(p,q)
% Errors," Journal of Econometrics 64, 183-206.

T=size(y_i,1); % number of time-series observations
k=size(x_i,2); % number of regressors
y_i_1_tilde=y_i(1:p_i,1); % first p_i observations for y_i
x_i_1_tilde=x_i(1:p_i,:); % first p_i observations for x_i
Sigma_i=Generate_Sigma_i(phi_i);
Q_i_t=chol(Sigma_i); % Sigma_i = Q_i_t'Q_i_t
Q_i_inv=inv(Q_i_t)'; % see OW, p. 1001
y_i_1_tilde_star=Q_i_inv*y_i_1_tilde;
x_i_1_tilde_star=Q_i_inv*x_i_1_tilde;
y_i_2_tilde_star=zeros(T-p_i,1);
x_i_2_tilde_star=zeros(T-p_i,k);
phi_i_reverse=flipud(phi_i);
for iter=1:(T-p_i);
    y_i_2_tilde_star(iter)=[-phi_i_reverse' 1]*y_i(iter:p_i+iter); % see OW, p. 1001
    x_i_2_tilde_star(iter,:)=[-phi_i_reverse' 1]*x_i(iter:p_i+iter,:);
end;
y_i_tilde_star=[y_i_1_tilde_star ; y_i_2_tilde_star]; % see OW, p. 1001
x_i_tilde_star=[x_i_1_tilde_star ; x_i_2_tilde_star];
B_i_bar=inv(B_i_bar_inv);
B_i=B_i_bar+(1/s2_i)*x_i_tilde_star'*x_i_tilde_star; % see OW, p. 1002
beta_i_hat=inv(B_i)*(B_i_bar*beta_i_bar+(1/s2_i)*x_i_tilde_star'*y_i_tilde_star);
beta_i_new=beta_i_hat+chol(inv(B_i))'*randn(k,1); % beta_i draw
