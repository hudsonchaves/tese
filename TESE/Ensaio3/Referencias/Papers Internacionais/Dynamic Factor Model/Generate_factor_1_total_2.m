function [factor_1_new]=Generate_factor_1_total_2(y,phi_f1,s2_f1,factors_2,...
    f2_select,loadings,phi,s2)

% Generate draw for first factor loading using eq. (9) in Otrok and Whiteman (1998).
%
% Calls Generate_S_i_inv
%
% Input:
%
% y         = matrix of observations for y_i (T by no_var)
% phi_f1    = p-vector of previous draw of AR coefficients for first factor
% s2_f1     = disturbance variance for first factor
% factors_2 = matrix of previous draws for second factors (T by no_f2)
% f2_select = no_var-vector to select appropriate second factor
% loadings  = matrix of previous draws for factor loadings (2 by no_var)
% phi       = matrix of previous draws for idiosyncratic AR coefficients (p by no_var)
% s2        = no_var-vector of previous draws for idiosyncratic disturbance variances
%
% Output:
%
% factor_1_new = T-vector of new draws of observations for first factor

no_var=size(y,2); % number of variables (n)
T=size(y,1); % number of time-series observations
S_f1_inv=Generate_S_i_inv(phi_f1,T); % S_i inverse; see OW, p. 1003
H=(1/s2_f1)*S_f1_inv'*S_f1_inv; % building H matrix; see OW, p. 1004
G=zeros(T,1);
for i=1:no_var;
    factors_2_i=factors_2(:,f2_select(i)); % appropriate factors for i
    beta_i_f1=loadings(1,i); % factor loadings for i
    beta_i_f2=loadings(2,i);
    phi_i=phi(:,i); % AR coefficients for idiosyncratic component of i
    s2_i=s2(i); % idiosyncratic disturbance variance for i
    S_i_inv=Generate_S_i_inv(phi_i,T); % S_i inverse; see OW, p. 1003
    y_i_star=S_i_inv*(y(:,i)-beta_i_f2*factors_2_i);
    C_i=beta_i_f1*S_i_inv;
    G=G+(1/s2_i)*C_i'*y_i_star;
    H=H+(1/s2_i)*C_i'*C_i; % see OW, p. 1004
end;
P=chol(H);
H_inv=inv(H); % posterior variance for first factor
mu=H_inv*G; % posterior mean for first factor
factor_1_new=mu+chol(H_inv)'*randn(T,1);
