function [VDecomp_i]=Generate_VDecomp_total_2(y_i,x_i)

% Computes a variance decomposition for one member of a 2-factor model.
%
% Input:
%
% y_i = vector of observations for y_i
% x_i = matrix of RHS variables (factors)
%
% Output:
%
% VDecomp_i = vector of shares (f1 ; f2 ; idiosyncratic)

VDecomp_i=zeros(3,1);
b_hat_vd=inv(x_i(:,1)'*x_i(:,1))*(x_i(:,1)'*x_i(:,2));
orth_2=x_i(:,2)-x_i(:,1)*b_hat_vd;
b_hat_vd=inv([x_i(:,1) orth_2]'*[x_i(:,1) orth_2])*([x_i(:,1) orth_2]'*y_i);
b_hat_vd=b_hat_vd.^2;
var_agg=(std(y_i)).^2;
VDecomp_i(1)=(b_hat_vd(1)*(std(x_i(:,1))^2))/var_agg;
VDecomp_i(2)=(b_hat_vd(2)*(std(orth_2)^2))/var_agg;
VDecomp_i(3)=1-VDecomp_i(1)-VDecomp_i(2);
