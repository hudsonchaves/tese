% Posterior_analysis_1951_2009_USA.m

clear;

% National factor, 4 regional factors 1951-2009

load('C:\Research\International_inflation\DFM_1951_2009_N1R4_USA_results');
G=size(sigma2s_store,2);

% Variance decompositions

mean_VDecomps=mean(VDecomps_store,3);
sort_VDecomps_store=sort(VDecomps_store,3);
VDecomps_05=sort_VDecomps_store(:,:,round(0.05*G));
VDecomps_95=sort_VDecomps_store(:,:,round(0.95*G));
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_VDecomps(1,:)' VDecomps_05(1,:)' VDecomps_95(1,:)'],'Decomps, 1951-2009, USA','c4:e21');
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_VDecomps(2,:)' VDecomps_05(2,:)' VDecomps_95(2,:)'],'Decomps, 1951-2009, USA','g4:i21');
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_VDecomps(3,:)' VDecomps_05(3,:)' VDecomps_95(3,:)'],'Decomps, 1951-2009, USA','k4:m21');
clear('VDecomps_store','sort_VDecomps_store');

% Factor loadings

mean_loadings=mean(loadings_store,3);
sort_loadings_store=sort(loadings_store,3);
loadings_05=sort_loadings_store(:,:,round(0.05*G));
loadings_95=sort_loadings_store(:,:,round(0.95*G));
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_loadings(1,:)' loadings_05(1,:)' loadings_95(1,:)'],'Area pars, 1951-2009, USA','c4:e21');
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_loadings(2,:)' loadings_05(2,:)' loadings_95(2,:)'],'Area pars, 1951-2009, USA','g4:i21');
clear('loadings_store','sort_loadings_store');

% City idiosyncractic AR parameters

mean_phis=mean(phis_store,3);
sort_phis_store=sort(phis_store,3);
phis_05=sort_phis_store(:,:,round(0.05*G));
phis_95=sort_phis_store(:,:,round(0.95*G));
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_phis(1,:)' phis_05(1,:)' phis_95(1,:)'],'Area pars, 1951-2009, USA','k4:m21');
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_phis(2,:)' phis_05(2,:)' phis_95(2,:)'],'Area pars, 1951-2009, USA','o4:q21');
clear('phis_store','sort_phis_store');

% City idiosyncratic variances

mean_sigma2s=mean(sigma2s_store,2);
sort_sigma2s_store=sort(sigma2s_store,2);
sigma2s_05=sort_sigma2s_store(:,round(0.05*G));
sigma2s_95=sort_sigma2s_store(:,round(0.95*G));
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [100*mean_sigma2s(:,1) 100*sigma2s_05(:,1) 100*sigma2s_95(:,1)],'Area pars, 1951-2009, USA','s4:u21');
clear('sigma2s_store','sort_sigma2s_store');

% National factor AR parameters

mean_phis_f1=mean(phis_f1_store,3);
sort_phis_f1_store=sort(phis_f1_store,3);
phis_f1_05=sort_phis_f1_store(:,:,round(0.05*G));
phis_f1_95=sort_phis_f1_store(:,:,round(0.95*G));
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_phis_f1(1,:)' phis_f1_05(1,:)' phis_f1_95(1,:)'],'Factor pars, 1951-2009, USA','b4:d4');
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_phis_f1(2,:)' phis_f1_05(2,:)' phis_f1_95(2,:)'],'Factor pars, 1951-2009, USA','f4:h4');
clear('phis_f1_store','sort_phis_f1_store');

% Regional factor AR parameters

mean_phis_f2=mean(phis_f2_store,3);
sort_phis_f2_store=sort(phis_f2_store,3);
phis_f2_05=sort_phis_f2_store(:,:,round(0.05*G));
phis_f2_95=sort_phis_f2_store(:,:,round(0.95*G));
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_phis_f2(1,:)' phis_f2_05(1,:)' phis_f2_95(1,:)'],'Factor pars, 1951-2009, USA','b5:d8');
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_phis_f2(2,:)' phis_f2_05(2,:)' phis_f2_95(2,:)'],'Factor pars, 1951-2009, USA','f5:h8');
clear('phis_f2_store','sort_phis_f2_store');
