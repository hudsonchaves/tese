function [phi_f_new]=Generate_phi_f(factor,q_f,phi_f_bar,V_f_bar_inv,phi_f,s2_f)

% Generates draw for phi_fk based on eq. (7) in Otrok and
% Whiteman (1998) which in turn is based on Chib and
% Greenberg (1994 Section 4.1). Note that the Metropolis-
% Hastings algorithm is also used. The notation closely
% follows Otrok and Whiteman (1998).
%
% Calls Generate_Sigma_i
%
% Input:
%
% factor      = vector of factors observations
% q_f         = AR order for factors process
% phi_f_bar   = prior mean of phi for factor process
% V_f_bar_inv = prior precision of phi for factor process
% phi_f       = previous factor loading draw for phi
% s2_f        = variance of factor disturbance
%
% Output:
%
% phi_f_new = new phi_f draw
%
% References:
%
% C. Otrok and C. Whiteman, "Bayesian Leading Indicators: Measuring and
% Predicting Economics Conditions in Iowa," International Economic Review
% 39, 997-1014
%
% S. Chib and E. Greenberg, "Bayes Inference in Regression Models with ARMA(p,q)
% Errors," Journal of Econometrics 64, 183-206.

V_f_bar=inv(V_f_bar_inv); % needed for definition of V_f
T=size(factor,1); % number of time-series observations
y_f=factor;
e_f_all=y_f;
e_f=e_f_all(q_f+1:T,1); % dropping first q_f observations (from q_f+1 to T) 
E_f=zeros(T-q_f,q_f); % constructing E_f, (T-q_f) by q_f; OW, p. 1001
for j=1:q_f;
    E_f(:,j)=e_f_all(q_f-(j-1):T-j);
end;
V_f=V_f_bar+(1/s2_f)*(E_f'*E_f); % V_f; OW, p. 1002
V_f_inv=inv(V_f); % variance component for posterior of phi_f; OW, p. 1002
phi_f_hat=inv(V_f)*(V_f_bar*phi_f_bar+(1/s2_f)*E_f'*e_f); % mean component; OW, p. 1002

% Ensuring phi_f meets stationarity condition and Sigma_f is positive-definite

accept=0;
iter=1;
while (accept<1);
    phi_f_new=phi_f_hat+chol(V_f_inv)'*randn(q_f,1); % candidate draw from normal
    SARC=abs(sum(phi_f_new)); % sum of AR coefficients
    Sigma_f_test=Generate_Sigma_i(phi_f_new);
    eig_Sigma_f_test=eig(Sigma_f_test); % checking positive-definiteness of Sigma_f
    if (SARC<1) & (min(eig_Sigma_f_test)>0);
        accept=1;
    else;
        accept=0;
        iter=iter+1;
        if iter>100;
            phi_f_new=phi_f;
            accept=1;
        end;
    end;
end;

% Numerator of acceptance probability for Metropolis-Hastings; see OW, p. 1002

y_f_1_tilde=y_f(1:q_f,1); % first q_f observations for y_f
Sigma_f_new=Generate_Sigma_i(phi_f_new);
Psi_f_new=(det(Sigma_f_new)^(-0.5))*exp(-0.5*(y_f_1_tilde'*inv(Sigma_f_new)*y_f_1_tilde)/s2_f);

% Denominator of acceptance probability for Metroplis-Hastings; see OW, p. 1002

Sigma_f_previous=Generate_Sigma_i(phi_f);
Psi_f_previous=(det(Sigma_f_previous)^(-0.5))*exp(-0.5*(y_f_1_tilde'*inv(Sigma_f_previous)*y_f_1_tilde)/s2_f);

% Acceptance criterion for Metropolis-Hastings

if Psi_f_previous==0;
    accept=1;
else;
    u=rand(1,1);
    accept=u<(Psi_f_new/Psi_f_previous);
end;
phi_f_new=phi_f_new*accept+phi_f*(1-accept);
