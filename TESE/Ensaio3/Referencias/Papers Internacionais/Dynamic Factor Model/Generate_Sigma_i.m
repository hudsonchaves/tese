function [Sigma_i]=Generate_Sigma_i(phi_i)

% Computes the Sigma_i matrix that is a function of
% the phi_i vector in Otrok and Whiteman (1998, p. 1001).
% This is the covariance matrix for the first p_i
% observations of y_it.
%
% Input:
%
% phi_i = p-vector of AR coefficients
%
% Output:
%
% sigma = Sigma_i matrix
%
% Reference:
%
% C. Otrok and C. Whiteman, "Bayesian Leading Indicators: Measuring and
% Predicting Economics Conditions in Iowa," International Economic Review
% 39, 997-1014

p_i=size(phi_i,1); % dimension of phi_i
if p_i>1;
    I=[eye(p_i-1) zeros(p_i-1,1)];
else;
    I=[];
end;
Psi_i=[phi_i' ; I];
I_Psi_Psi=eye(p_i^2)-kron(Psi_i,Psi_i);
if p_i>1;
    e1=[1 ; zeros(p_i-1,1)];
else;
    e1=1;
end;
vec_Sigma_i=inv(I_Psi_Psi)*reshape(e1*e1',p_i^2,1);
Sigma_i=reshape(vec_Sigma_i,p_i,p_i);
