% DFM_1951_1979_W1R7.m

clear;

% Loading raw data, 1950:1-2009:4 (64 countries)

Data_GFD_CPI; 
CPI=data_GFD;
N=size(CPI,2); % number of countries

% Fixing scale of CPI for some countries

CPI(1:136,1)=(1/1000000)*CPI(1:136,1); % Argentina, 1950:1-1983:4
CPI(1:140,5)=(1/1000000)*CPI(1:140,5); % Bolivia, 1950:1-1984:4
CPI(1:140,6)=(1/(1000000*1000000))*CPI(1:140,6); % Brazil, 1950:1-1984:4
CPI(141:168,6)=(1/1000000)*CPI(141:168,6); % Brazil, 1985:1-1991:4
CPI(1:100,10)=(1/1000000)*CPI(1:100,10); % Chile, 1950:1-1974:4
CPI(1:160,50)=(1/1000000)*CPI(1:160,50); % Peru, 1950:1-1984:4
CPI(1:148,61)=(1/1000000)*CPI(1:148,61); % Uruguay, 1950:1-1986:4 

% Computing annual inflation rates, 1951-2009

number_years=size(CPI,1)/4;
Inflation=zeros(number_years-1,N);
for i=1:(number_years-1);
    Inflation(i,:)=log(CPI(4*(i+1),:))-log(CPI(4*i,:));
end;
Inflation=Inflation(1:29,:); % 1951-1979

% Constructing regions

NA_column_nos=[7 8 58 62]; % North America
NA_data=Inflation(:,NA_column_nos);
LA_column_nos=[1 5 6 10 12 13 17 18 26 27 36 42 50 53 61 63]; % Latin America
LA_data=Inflation(:,LA_column_nos);
EU_column_nos=[3 4 9 14 15 16 20 21 23 24 25 31 33 35 41 43 46 47 52 56]; % Europe
EU_data=Inflation(:,EU_column_nos);
AF_column_nos=[11 38 44 54 64]; % Africa
AF_data=Inflation(:,AF_column_nos);
AS_column_nos=[28 29 30 37 39 40 45 51 55 57]; % Asia
AS_data=Inflation(:,AS_column_nos);
ME_column_nos=[19 32 34 49 59 60]; % Middle East
ME_data=Inflation(:,ME_column_nos);
OC_column_nos=[2 22 48]; % Oceania
OC_data=Inflation(:,OC_column_nos);

% Creating data matrix for all countries

World_data=[NA_data LA_data EU_data AF_data AS_data ME_data OC_data];
y=World_data;
stats=[mean(y)' std(y)' min(y)' max(y)']; % summary stats
y=y-kron(mean(y),ones(size(y,1),1)); % de-meaned data

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Code constructed from Kose et al. (2003) GAUSS code
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% NB: We treat first factor as the aggregate (world) factor,
% second factor as the regional factor, and third factor as
% the country factor

% Preliminaries

T=size(y,1); % sample length
N=size(y,2); % number of cross-sectional units
no_var=N; % total number of observable variables
no_f1=1; % number of first factors
no_f2=7; % number of second factors
region_size=[4 16 20 5 10 6 3]; % number of countries in each region
no_burnin=1000; % number of draws to leave out (burn-in portion)
m=10000; % Gibbs sample size
k=2; % number of independent variables for each i
p=2; % AR order for idiosyncratic shocks; see (2)
q=2; % AR orders for factor processes; see (4)

% Defining priors for factor loadings

beta_mean_prior=zeros(k,1); % prior mean for beta_i (factor loadings)
beta_var_prior=eye(k); % prior variance for beta_i

% Defining priors for idiosyncratic AR terms

diagonal_terms=1;
if p>1;
    powers=1:(p-1);
    powers=powers';
    diagonal_terms=[1 ;(0.5*ones(p-1,1)).^powers]; % see p. 1221
end;
phi_i_mean_prior=zeros(p,1); % prior mean of phi for idiosyncratic part
phi_i_var_prior=diag(diagonal_terms); % prior variance of phi for idiosyncratic part

% Defining priors for factor AR terms

diagonal_terms=1;
if q>1;
    powers=1:(q-1);
    powers=powers';
    diagonal_terms=[1;(0.5*ones(q-1,1)).^powers]; % see p. 1221
end;
phi_f_mean_prior=zeros(q,1); % prior mean of phi for factor process
phi_f_var_prior=diag(diagonal_terms); % prior variance for phi for factor process

% Defining priors for idiosyncratic shock variance

v_bar=6*2; % inverted gamma parameter 1 for idiosyncratic variance
delta_bar=0.001*2; % inverted gamma parameter 2 for idiosyncratic variance

% Generating initial values for factors (drawing from normal distribution)

% NB: s2_f1 and s2_f2are set by the user and correspond to the assumed
% value of the shock variance for each factor; see KOW, p. 1219, penultimate paragraph

s2_f1=ones(1,no_f1); % row vector of variances for first factor
s2_f2=ones(1,no_f2); % row vector of variances for second factors
s_f1=sqrt(s2_f1);
s_f2=sqrt(s2_f2);
factors_1_0=randn(T,no_f1).*repmat(s_f1,T,1); % initial draw of first factors
factors_2_0=randn(T,no_f2).*repmat(s_f2,T,1); % initial draw of second factors

% Creating vectors to select appropriate factors for each variable

f1_select=ones(no_var,1); % selecting relevant first factor
f2_select=[]; % selecting relevant second factor
for i=1:size(region_size,2);
    f2_select=[f2_select ; i*ones(region_size(i),1)];
end;

% Constructing vectors used to impose sign restrictions on factor loadings

restrict_1=zeros(no_var,1);
restrict_1(1)=1; % require first loading to be positive for this unit
restrict_2=zeros(no_var,1);
restrict_2([1 5 21 41 46 56 62]')=1; % require second loading to be positive for these units

% Generating initial values for parameters (use OLS estimates for factor loadings and sigma^2_i)

loadings_0=zeros(k,no_var); % initial values for factor loadings
sigma2s_0=zeros(no_var,1); % initial values for sigma^2_i
for i=1:no_var;
    X=[factors_1_0(1:T,f1_select(i)) factors_2_0(1:T,f2_select(i))];
    beta_i_0=X\y(:,i); % factor loadings for i
    loadings_0(:,i)=beta_i_0;
    sigma2s_0(i)=((y(:,i)-X*beta_i_0)'*(y(:,i)-X*beta_i_0)/(T-k));
end;

% Creating matrices to store draws of factors and parameters

factors_1_store=NaN([size(factors_1_0),no_burnin+m]); % for storing first factors
factors_2_store=NaN([size(factors_2_0),no_burnin+m]); % for storing second factors
loadings_store=NaN([size(loadings_0),no_burnin+m]); % for storing factor loadings
sigma2s_store=NaN([size(sigma2s_0,1),no_burnin+m]); % for storing sigma2_is
phis_store=NaN(p,no_var,no_burnin+m); % for storing phi_is
phis_f1_store=NaN(q,no_f1,no_burnin+m); % for storing phi_f1s
phis_f2_store=NaN(q,no_f2,no_burnin+m); % for storing phi_f2s
VDecomps_store=NaN(k+1,no_var,no_burnin+m); % for storing variance decompositions
sign_switch_store=NaN(no_burnin+m,1);
factors_1_store(:,:,1)=factors_1_0;
factors_2_store(:,:,1)=factors_2_0;
loadings_store(:,:,1)=loadings_0;
sigma2s_store(:,1)=sigma2s_0;
phis_store(:,:,1)=zeros(p,no_var);
phis_f1_store(:,:,1)=zeros(q,no_f1);
phis_f2_store(:,:,1)=zeros(q,no_f2);
VDecomps_store(:,:,1)=zeros(k+1,no_var);
sign_switch_store(1)=0;

% Priors (purely for notation to match OW)

beta_i_bar=beta_mean_prior;
B_i_bar_inv=beta_var_prior;
phi_i_bar=phi_i_mean_prior;
V_i_bar_inv=phi_i_var_prior;
v_i_bar=v_bar;
delta_i_bar=delta_bar;
p_i=p;
phi_f_bar=phi_f_mean_prior;
V_f_bar_inv=phi_f_var_prior;
q_f=q;

% Generating draws for Gibb sampling

for iter=2:no_burnin+m;

    % Drawing phi_i, beta_i, sigma2_i, and performing variance decomposition

    phis=zeros(p_i,no_var);
    loadings=zeros(k,no_var);
    sigma2s=zeros(no_var,1);
    VDecomps=zeros(k+1,no_var);
    sign_switch=0;
    for i=1:no_var;
        y_i=y(:,i);
        factors_1_i=factors_1_store(:,f1_select(i),iter-1);
        factors_2_i=factors_2_store(:,f2_select(i),iter-1);
        x_i=[factors_1_i factors_2_i];
        beta_i=loadings_store(:,i,iter-1);
        sigma2_i=sigma2s_store(i,iter-1);
        phi_i=phis_store(:,i,iter-1);
        [phi_i_new]=Generate_phi_i(y(:,i),x_i,p_i,phi_i_bar,V_i_bar_inv,beta_i,sigma2_i,phi_i);
        if ((restrict_1(i)==1) & (restrict_2(i)==1));
            sign_restrict=1;
            attempts=1;
            while ((sign_restrict==1) & (attempts <= 50));
                [beta_i_new]=Generate_beta_i(y_i,x_i,p_i,phi_i_new,beta_i_bar,B_i_bar_inv,sigma2_i);
                if ((beta_i_new(1)>0) & (beta_i_new(2)>0));
                    sign_restrict=-1; % sign restriction satisfied
                else;
                    attempts=attempts+1;
                end;
            end;
            if beta_i_new(1)<0;
                beta_i_new(1)=-beta_i_new(1);
                factors_1_store(:,f1_select(i),iter-1)=-factors_1_store(:,f1_select(i),iter-1);
                sign_switch=sign_switch+1;
            end;
            if beta_i_new(2)<0;
                beta_i_new(2)=-beta_i_new(2);
                factors_2_store(:,f2_select(i),iter-1)=-factors_2_store(:,f2_select(i),iter-1);
                sign_switch=sign_switch+1;
            end;
        elseif ((restrict_1(i)==0) & (restrict_2(i)==1));
            sign_restrict=1;
            attempts=1;
            while ((sign_restrict==1) & (attempts <= 50));
                [beta_i_new]=Generate_beta_i(y_i,x_i,p_i,phi_i_new,beta_i_bar,B_i_bar_inv,sigma2_i);
                if beta_i_new(2)>0;
                    sign_restrict=-1; % sign restriction satisfied
                else;
                    attempts=attempts+1;
                end;
            end;
            if beta_i_new(2)<0;
                beta_i_new(2)=-beta_i_new(2);
                factors_2_store(:,f2_select(i),iter-1)=-factors_2_store(:,f2_select(i),iter-1);
                sign_switch=sign_switch+1;
            end;
        else;
            [beta_i_new]=Generate_beta_i(y_i,x_i,p_i,phi_i_new,beta_i_bar,B_i_bar_inv,sigma2_i);
        end;
        [sigma2_i_new]=Generate_sigma2_i(y_i,x_i,p_i,phi_i_new,beta_i_new,v_i_bar,delta_i_bar);
        [VDecomp_i_new]=Generate_VDecomp_total_2(y_i,x_i);
        phis(:,i)=phi_i_new;
        loadings(:,i)=beta_i_new;
        sigma2s(i)=sigma2_i_new;
        VDecomps(:,i)=VDecomp_i_new;
        disp([iter i]);
    end;

    % Drawing factors

    factors_1_new=Generate_factor_1_total_2(y,phis_f1_store(:,:,iter-1),s2_f1,factors_2_store(:,:,iter-1),...
        f2_select,loadings,phis,sigma2s);
    factors_2_new=Generate_factors_2_total_2(no_f2,y,phis_f2_store(:,:,iter-1),s2_f2,factors_1_new,...
        f2_select,loadings,phis,sigma2s);

    % Drawing phi_f

    phis_f1=zeros(q_f,no_f1);
    phis_f2=zeros(q_f,no_f2);
    phi_f1_new=Generate_phi_f(factors_1_new,q_f,phi_f_bar,V_f_bar_inv,phis_f1_store(:,:,iter-1),s2_f1);
    phis_f1=phi_f1_new;
    for l=1:no_f2;
        phi_f2_l_new=Generate_phi_f(factors_2_new(:,l),q_f,phi_f_bar,V_f_bar_inv,...
            phis_f2_store(:,l,iter-1),s2_f2(l));
        phis_f2(:,l)=phi_f2_l_new;
    end;

    % Storing new draws

    factors_1_store(:,:,iter)=factors_1_new; 
    factors_2_store(:,:,iter)=factors_2_new; 
    loadings_store(:,:,iter)=loadings; 
    sigma2s_store(:,iter)=sigma2s; 
    phis_store(:,:,iter)=phis; 
    phis_f1_store(:,:,iter)=phis_f1; 
    phis_f2_store(:,:,iter)=phis_f2; 
    VDecomps_store(:,:,iter)=VDecomps; 
    sign_switch_store(iter)=sign_switch;
end;

% Getting post-burn-in posterior draws

phis_store=phis_store(:,:,no_burnin+1:no_burnin+m);
loadings_store=loadings_store(:,:,no_burnin+1:no_burnin+m);
sigma2s_store=sigma2s_store(:,no_burnin+1:no_burnin+m);
VDecomps_store=VDecomps_store(:,:,no_burnin+1:no_burnin+m);
factors_1_store=factors_1_store(:,:,no_burnin+1:no_burnin+m);
factors_2_store=factors_2_store(:,:,no_burnin+1:no_burnin+m);
phis_f1_store=phis_f1_store(:,:,no_burnin+1:no_burnin+m);
phis_f2_store=phis_f2_store(:,:,no_burnin+1:no_burnin+m);
sign_switch_store=sign_switch_store(no_burnin+1:no_burnin+m);

% Computing posterior means

mean_phis=mean(phis_store,3);
mean_loadings=mean(loadings_store,3);
mean_sigma2s=mean(sigma2s_store,2);
mean_VDecomps=mean(VDecomps_store,3);
mean_factors_1=mean(factors_1_store,3);
mean_factors_2=mean(factors_2_store,3);
mean_phis_f1=mean(phis_f1_store,3);
mean_phis_f2=mean(phis_f2_store,3);

% Saving post-burn-in draws

save('DFM_1951_1979_W1R7_results','stats',...
    'VDecomps_store','loadings_store','phis_store','sigma2s_store',...
    'factors_1_store','factors_2_store','phis_f1_store','phis_f2_store');
