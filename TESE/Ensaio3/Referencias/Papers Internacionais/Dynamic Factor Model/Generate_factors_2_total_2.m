function [factors_2_new]=Generate_factors_2_total_2(no_f2,y,phi_f2,s2_f2,factor_1,...
    f2_select,loadings,phi,s2)

% Generate draw for set of second factors using eq. (9) in Otrok and Whiteman (1998).
%
% Calls Generate_S_i_inv
%
% Input:
%
% no_f2     = number of second factors
% y         = matrix of observations for y_i (T by no_var)
% phi_f2    = matrix of previous draws for AR coefficients for second factors
% s2_f2     = no_f2-vector of disturbance variances for second factors
% factor_1  = T-vector of previous draws for first factors
% f2_select = no_var-vector to select appropriate second factor
% loadings  = matrix of previous draws for factor loadings (3 by no_var)
% phi       = matrix of previous draws for idiosyncratic AR coefficients (p by no_var)
% s2        = no_var-vector of previous draws for idiosyncratic disturbance variances
%
% Output:
%
% factors_2_new = matrix of new draws of observations for second factors (T by no_f3)

no_var=size(y,2); % number of variables (n)
T=size(y,1); % number of time-series observations
factors_2_new=zeros(T,no_f2);
for j=1:no_f2;
    S_f2_j_inv=Generate_S_i_inv(phi_f2(:,j),T);
    H_j=(1/s2_f2(j))*S_f2_j_inv'*S_f2_j_inv;
    G_j=zeros(T,1);
    i=1;
    while i<no_var+1;
        if f2_select(i)==j;
            beta_i_f1=loadings(1,i); % factor loadings for i
            beta_i_f2=loadings(2,i);
            phi_i=phi(:,i); % AR coefficients for idiosyncratic component of i
            s2_i=s2(i); % idiosyncratic disturbance variance for i        
            S_i_inv=Generate_S_i_inv(phi_i,T); % S_i inverse; see OW, p. 1003
            y_i_star=S_i_inv*(y(:,i)-beta_i_f1*factor_1);
            C_i=beta_i_f2*S_i_inv;
            G_j=G_j+(1/s2_i)*C_i'*y_i_star;
            H_j=H_j+(1/s2_i)*C_i'*C_i; % see OW, p. 1004
            i=i+1;
        else;
            i=i+1;
        end;
    end;
    P_j=chol(H_j);
    H_j_inv=inv(H_j); % posterior variance for second factor
    mu_j=H_j_inv*G_j; % posterior mean for second factor
    factors_2_new(:,j)=mu_j+chol(H_j_inv)'*randn(T,1);
end;
