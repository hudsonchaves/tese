function [S_i_inv]=Generate_S_i_inv(phi_i,T);

% Generates S_i inverse matrix given on p. 1003 in Otrok
% and Whiteman (1998). This is needed for the factor draws.
%
% Calls Generate_Sigma_i
%
% Input:
%
% phi_i = p-vector of AR coefficients
%
% Output:
%
% S_i_inv = S_i inverse matrix
%
% Reference:
%
% C. Otrok and C. Whiteman, "Bayesian Leading Indicators: Measuring and
% Predicting Economics Conditions in Iowa," International Economic Review
% 39, 997-1014

p_i=size(phi_i,1); % dimension of phi_i
Sigma_i=Generate_Sigma_i(phi_i); % generating Sigma_i to comput Q_i
Q_i_t=chol(Sigma_i); % Sigma_i = Q_i_t'Q_i_t
Q_i_inv=inv(Q_i_t)'; % see OW, p. 1001
S_i_inv_upper=[Q_i_inv zeros(p_i,T-p_i)];
phi_i_reverse=flipud(phi_i);
S_i_inv_lower=zeros(T-p_i,T);
for q=1:T-p_i;
    S_i_inv_lower(q,q:q+p_i)=[phi_i_reverse' 1];
end;
S_i_inv=[S_i_inv_upper ; S_i_inv_lower];
