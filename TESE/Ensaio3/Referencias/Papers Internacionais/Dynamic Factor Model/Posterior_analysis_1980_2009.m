% Posterior_analysis_1980_2009.m

clear;

% World factor, 7 regional factors 1980-2009

load('C:\Research\International_inflation\DFM_1980_2009_W1R7_results');
G=size(sigma2s_store,2);

% Variance decompositions

mean_VDecomps=mean(VDecomps_store,3);
sort_VDecomps_store=sort(VDecomps_store,3);
VDecomps_05=sort_VDecomps_store(:,:,round(0.05*G));
VDecomps_95=sort_VDecomps_store(:,:,round(0.95*G));
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_VDecomps(1,:)' VDecomps_05(1,:)' VDecomps_95(1,:)'],'Decompositions, 1980-2009','c4:e67');
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_VDecomps(2,:)' VDecomps_05(2,:)' VDecomps_95(2,:)'],'Decompositions, 1980-2009','g4:i67');
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_VDecomps(3,:)' VDecomps_05(3,:)' VDecomps_95(3,:)'],'Decompositions, 1980-2009','k4:m67');
clear('VDecomps_store','sort_VDecomps_store');

% Factor loadings

mean_loadings=mean(loadings_store,3);
sort_loadings_store=sort(loadings_store,3);
loadings_05=sort_loadings_store(:,:,round(0.05*G));
loadings_95=sort_loadings_store(:,:,round(0.95*G));
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_loadings(1,:)' loadings_05(1,:)' loadings_95(1,:)'],'Country parameters, 1980-2009','c4:e67');
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_loadings(2,:)' loadings_05(2,:)' loadings_95(2,:)'],'Country parameters, 1980-2009','g4:i67');
clear('loadings_store','sort_loadings_store');

% World factor

mean_factors_1=mean(factors_1_store,3);
sort_factors_1_store=sort(factors_1_store,3);
factors_1_05=sort_factors_1_store(:,:,round(0.05*G));
factors_1_95=sort_factors_1_store(:,:,round(0.95*G));
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_factors_1(:,1) factors_1_05(:,1) factors_1_95(:,1)],'Factors, 1980-2009','b4:d33');
clear('factors_1_store','sort_factors_1_store');

% Regional factors

mean_factors_2=mean(factors_2_store,3);
sort_factors_2_store=sort(factors_2_store,3);
factors_2_05=sort_factors_2_store(:,:,round(0.05*G));
factors_2_95=sort_factors_2_store(:,:,round(0.95*G));
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_factors_2(:,1) factors_2_05(:,1) factors_2_95(:,1)],'Factors, 1980-2009','f4:h33');
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_factors_2(:,2) factors_2_05(:,2) factors_2_95(:,2)],'Factors, 1980-2009','j4:l33');
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_factors_2(:,3) factors_2_05(:,3) factors_2_95(:,3)],'Factors, 1980-2009','n4:p33');
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_factors_2(:,4) factors_2_05(:,4) factors_2_95(:,4)],'Factors, 1980-2009','r4:t33');
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_factors_2(:,5) factors_2_05(:,5) factors_2_95(:,5)],'Factors, 1980-2009','v4:x33');
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_factors_2(:,6) factors_2_05(:,6) factors_2_95(:,6)],'Factors, 1980-2009','z4:ab33');
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [mean_factors_2(:,7) factors_2_05(:,7) factors_2_95(:,7)],'Factors, 1980-2009','ad4:af33');
clear('factors_2_store','sort_factors_2_store');
