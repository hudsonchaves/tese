function [phi_i_new]=Generate_phi_i(y_i,x_i,p_i,phi_i_bar,V_i_bar_inv,beta_i,s2_i,phi_i)

% Generates draw for phi_i based on eq. (7) in Otrok and
% Whiteman (1998) which in turn is based on Chib and
% Greenberg (1994 Section 4.1). Note that the Metropolis-
% Hastings algorithm is also used. The notation closely
% follows Otrok and Whiteman (1998).
%
% Calls Generate_Sigma_i
%
% Input:
%
% y_i         = vector of observations for y_i
% x_i         = matrix of RHS variables (previous factor draws) for i
% p_i         = AR order for idiosyncratic shock for i
% phi_i_bar   = prior mean of phi for idiosyncratic part
% V_i_bar_inv = prior precision of phi for idiosyncratic part
% beta_i      = previous factor loading draw for i
% s2_i        = previous idiosyncratic variance draw for i
% phi_i       = previous idiosyncratic AR coefficient draw for i
%
% Output:
%
% phi_i_new = new phi_i draw
%
% References:
%
% C. Otrok and C. Whiteman, "Bayesian Leading Indicators: Measuring and
% Predicting Economics Conditions in Iowa," International Economic Review
% 39, 997-1014
%
% S. Chib and E. Greenberg, "Bayes Inference in Regression Models with ARMA(p,q)
% Errors," Journal of Econometrics 64, 183-206.

V_i_bar=inv(V_i_bar_inv); % needed for definition of V_i
T=size(y_i,1); % number of time-series observations
e_i_all=y_i-x_i*beta_i; % vector of residuals for idiosyncratic component
e_i=e_i_all(p_i+1:T,1); % dropping first p_i observations (from p_i+1 to T) 
E_i=zeros(T-p_i,p_i); % constructing E_i; OW, p. 1001
for j=1:p_i;
    E_i(:,j)=e_i_all(p_i-(j-1):T-j);
end;
V_i=V_i_bar+(1/s2_i)*(E_i'*E_i); % V_i; OW, p. 1002
V_i_inv=inv(V_i); % variance component for posterior of phi_i; OW, p. 1002
phi_i_hat=inv(V_i)*(V_i_bar*phi_i_bar+(1/s2_i)*E_i'*e_i); % mean component; OW, p. 1002

% Ensuring phi_i meets stationarity condition and Sigma_i is positive-definite

accept=0;
iter=1;
while (accept<1);
    phi_i_new=phi_i_hat+chol(V_i_inv)'*randn(p_i,1); % candidate draw from normal
    SARC=abs(sum(phi_i_new)); % sum of AR coefficients
    Sigma_i_test=Generate_Sigma_i(phi_i_new);
    eig_Sigma_i_test=eig(Sigma_i_test); % checking positive-definiteness of Sigma_i
    if (SARC<1) & (min(eig_Sigma_i_test)>0);
        accept=1;
    else;
        accept=0;
        iter=iter+1;
        if iter>100;
            phi_i_new=phi_i;
            accept=1;
        end;
    end;
end;

% Numerator of acceptance probability for Metropolis-Hastings; see OW, p. 1002

y_i_1_tilde=y_i(1:p_i,1); % first p_i observations for y_i
x_i_1_tilde=x_i(1:p_i,:); % first p_i observations for x_i
Sigma_i_new=Generate_Sigma_i(phi_i_new);
Psi_i_new=(det(Sigma_i_new)^(-0.5))*exp(-0.5*(y_i_1_tilde-x_i_1_tilde*beta_i)'*...
    inv(Sigma_i_new)*(y_i_1_tilde-x_i_1_tilde*beta_i)/s2_i);

% Denominator of acceptance probability for Metroplis-Hastings; see OW, p. 1002

Sigma_i_previous=Generate_Sigma_i(phi_i);
Psi_i_previous=(det(Sigma_i_previous)^(-0.5))*exp(-0.5*(y_i_1_tilde-x_i_1_tilde*beta_i)'*...
    inv(Sigma_i_previous)*(y_i_1_tilde-x_i_1_tilde*beta_i)/s2_i);

% Acceptance criterion for Metropolis-Hastings

if Psi_i_previous==0;
    accept=1;
else;
    u=rand(1,1);
    accept=u<(Psi_i_new/Psi_i_previous);
end;
phi_i_new=phi_i_new*accept+phi_i*(1-accept);
