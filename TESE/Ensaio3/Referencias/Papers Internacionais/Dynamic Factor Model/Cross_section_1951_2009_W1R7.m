% Cross_section_1951_2006_W1R7.m

clear;

% Loading data, regressands

world_fraction=xlsread('International_inflation_results',...
    'Decompositions, 1951-2009','c4:c67');
region_fraction=xlsread('International_inflation_results',...
    'Decompositions, 1951-2009','g4:g67');
country_fraction=xlsread('International_inflation_results',...
    'Decompositions, 1951-2009','k4:k67');
N=size(world_fraction); % number of countries

% Openness

sw_open=xlsread('Data_country_characteristics','Full sample',...
    'd4:d67'); % openness index
missing=[33 ; 35 ; 36 ; 63];
sw_open_drop=sw_open;
sw_open_drop(missing,:)=[];
world_fraction_drop=world_fraction;
region_fraction_drop=region_fraction;
country_fraction_drop=country_fraction;
world_fraction_drop(missing,:)=[];
region_fraction_drop(missing,:)=[];
country_fraction_drop(missing,:)=[];
Y_world_sw_open=world_fraction_drop;
Y_region_sw_open=region_fraction_drop;
Y_country_sw_open=country_fraction_drop;
X_sw_open=[ones(size(Y_world_sw_open,1),1) sw_open_drop];
results_wf_sw_open=white_robust(Y_world_sw_open,X_sw_open);
results_rf_sw_open=white_robust(Y_region_sw_open,X_sw_open);
results_cf_sw_open=white_robust(Y_country_sw_open,X_sw_open);
disp('Y = world fraction; X = constant, openness');
prt(results_wf_sw_open);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_wf_sw_open.beta(2) results_wf_sw_open.tstat(2) ...
    results_wf_sw_open.nobs results_wf_sw_open.rbar],...
    'Cross-section, 1951-2009','b7:e7');
disp('Y = region fraction; X = constant, openness');
prt(results_rf_sw_open);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_rf_sw_open.beta(2) results_rf_sw_open.tstat(2) ...
    results_rf_sw_open.nobs results_rf_sw_open.rbar],...
    'Cross-section, 1951-2009','b18:e18');
disp('Y = country fraction; X = constant, openness');
prt(results_cf_sw_open);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_cf_sw_open.beta(2) results_cf_sw_open.tstat(2) ...
    results_cf_sw_open.nobs results_cf_sw_open.rbar],...
    'Cross-section, 1951-2009','b29:e29');

% Institutions

sw_institutions=xlsread('Data_country_characteristics','Full sample',...
    'e4:e67'); % index of institution quality
missing=[1 ; 24 ; 33 ; 35 ; 36 ; 43 ; 46 ; 57 ; 63];
sw_institutions_drop=sw_institutions;
sw_institutions_drop(missing,:)=[];
world_fraction_drop=world_fraction;
region_fraction_drop=region_fraction;
country_fraction_drop=country_fraction;
world_fraction_drop(missing,:)=[];
region_fraction_drop(missing,:)=[];
country_fraction_drop(missing,:)=[];
Y_world_sw_institutions=world_fraction_drop;
Y_region_sw_institutions=region_fraction_drop;
Y_country_sw_institutions=country_fraction_drop;
X_sw_institutions=[ones(size(Y_world_sw_institutions,1),1) sw_institutions_drop];
results_wf_sw_institutions=white_robust(Y_world_sw_institutions,X_sw_institutions);
results_rf_sw_institutions=white_robust(Y_region_sw_institutions,X_sw_institutions);
results_cf_sw_institutions=white_robust(Y_country_sw_institutions,X_sw_institutions);
disp('Y = world fraction; X = constant, institutions');
prt(results_wf_sw_institutions);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_wf_sw_institutions.beta(2) results_wf_sw_institutions.tstat(2) ...
    results_wf_sw_institutions.nobs results_wf_sw_institutions.rbar],...
    'Cross-section, 1951-2009','b8:e8');
disp('Y = region fraction; X = constant, institutions');
prt(results_rf_sw_institutions);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_rf_sw_institutions.beta(2) results_rf_sw_institutions.tstat(2) ...
    results_rf_sw_institutions.nobs results_rf_sw_institutions.rbar],...
    'Cross-section, 1951-2009','b19:e19');
disp('Y = country fraction; X = constant, institutions');
prt(results_cf_sw_institutions);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_cf_sw_institutions.beta(2) results_cf_sw_institutions.tstat(2) ...
    results_cf_sw_institutions.nobs results_cf_sw_institutions.rbar],...
    'Cross-section, 1951-2009','b30:e30');

% Liquid liabilities

liquid=xlsread('Data_country_characteristics','Full sample',...
    'j4:j67'); %
missing=[35 ; 41 ; 46 ; 47 ; 54 ; 56 ; 57 ; 60 ; 61];
liquid_drop=liquid;
liquid_drop(missing,:)=[];
world_fraction_drop=world_fraction;
region_fraction_drop=region_fraction;
country_fraction_drop=country_fraction;
world_fraction_drop(missing,:)=[];
region_fraction_drop(missing,:)=[];
country_fraction_drop(missing,:)=[];
Y_world_liquid=world_fraction_drop;
Y_region_liquid=region_fraction_drop;
Y_country_liquid=country_fraction_drop;
X_liquid=[ones(size(Y_world_liquid,1),1) liquid_drop];
results_wf_liquid=white_robust(Y_world_liquid,X_liquid);
results_rf_liquid=white_robust(Y_region_liquid,X_liquid);
results_cf_liquid=white_robust(Y_country_liquid,X_liquid);
disp('Y = world fraction; X = constant, liquid liabilities');
prt(results_wf_liquid);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_wf_liquid.beta(2) results_wf_liquid.tstat(2) ...
    results_wf_liquid.nobs results_wf_liquid.rbar],...
    'Cross-section, 1951-2009','b9:e9');
disp('Y = region fraction; X = constant, liquid liabilities');
prt(results_rf_liquid);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_rf_liquid.beta(2) results_rf_liquid.tstat(2) ...
    results_rf_liquid.nobs results_rf_liquid.rbar],...
    'Cross-section, 1951-2009','b20:e20');
disp('Y = country fraction; X = constant, liquid liabilities');
prt(results_cf_liquid);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_cf_liquid.beta(2) results_cf_liquid.tstat(2) ...
    results_cf_liquid.nobs results_cf_liquid.rbar],...
    'Cross-section, 1951-2009','b31:e31');

% Government share

gov_share=xlsread('Data_country_characteristics','Full sample',...
    'r4:r67'); %
gov_share_drop=gov_share;
world_fraction_drop=world_fraction;
region_fraction_drop=region_fraction;
country_fraction_drop=country_fraction;
Y_world_gov_share=world_fraction_drop;
Y_region_gov_share=region_fraction_drop;
Y_country_gov_share=country_fraction_drop;
X_gov_share=[ones(size(Y_world_gov_share,1),1) gov_share_drop];
results_wf_gov_share=white_robust(Y_world_gov_share,X_gov_share);
results_rf_gov_share=white_robust(Y_region_gov_share,X_gov_share);
results_cf_gov_share=white_robust(Y_country_gov_share,X_gov_share);
disp('Y = world fraction; X = constant, government share');
prt(results_wf_gov_share);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_wf_gov_share.beta(2) results_wf_gov_share.tstat(2) ...
    results_wf_gov_share.nobs results_wf_gov_share.rbar],...
    'Cross-section, 1951-2009','b10:e10');
disp('Y = region fraction; X = constant, government share');
prt(results_rf_gov_share);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_rf_gov_share.beta(2) results_rf_gov_share.tstat(2) ...
    results_rf_gov_share.nobs results_rf_gov_share.rbar],...
    'Cross-section, 1951-2009','b21:e21');
disp('Y = country fraction; X = constant, government share');
prt(results_cf_gov_share);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_cf_gov_share.beta(2) results_cf_gov_share.tstat(2) ...
    results_cf_gov_share.nobs results_cf_gov_share.rbar],...
    'Cross-section, 1951-2009','b32:e32');

% GDP per capita

gdp=xlsread('Data_country_characteristics','Full sample',...
    'q4:q67'); % 
gdp_drop=log(gdp);
world_fraction_drop=world_fraction;
region_fraction_drop=region_fraction;
country_fraction_drop=country_fraction;
Y_world_gdp=world_fraction_drop;
Y_region_gdp=region_fraction_drop;
Y_country_gdp=country_fraction_drop;
X_gdp=[ones(size(Y_world_gdp,1),1) gdp_drop];
results_wf_gdp=white_robust(Y_world_gdp,X_gdp);
results_rf_gdp=white_robust(Y_region_gdp,X_gdp);
results_cf_gdp=white_robust(Y_country_gdp,X_gdp);
disp('Y = world fraction; X = constant, log(avg. real GDP per capita)');
prt(results_wf_gdp);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_wf_gdp.beta(2) results_wf_gdp.tstat(2) ...
    results_wf_gdp.nobs results_wf_gdp.rbar],...
    'Cross-section, 1951-2009','b11:e11');
disp('Y = region fraction; X = constant, log(avg. real GDP per capita)');
prt(results_rf_gdp);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_rf_gdp.beta(2) results_rf_gdp.tstat(2) ...
    results_rf_gdp.nobs results_rf_gdp.rbar],...
    'Cross-section, 1951-2009','b22:e22');
disp('Y = country fraction; X = constant, log(avg. real GDP per capita)');
prt(results_cf_gdp);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_cf_gdp.beta(2) results_cf_gdp.tstat(2) ...
    results_cf_gdp.nobs results_cf_gdp.rbar],...
    'Cross-section, 1951-2009','b33:e33');

% Average inflation

inflation=xlsread('Data_country_characteristics','Full sample',...
    'w4:w67'); % 
inflation_drop=inflation;
world_fraction_drop=world_fraction;
region_fraction_drop=region_fraction;
country_fraction_drop=country_fraction;
Y_world_inflation=world_fraction_drop;
Y_region_inflation=region_fraction_drop;
Y_country_inflation=country_fraction_drop;
X_inflation=[ones(size(Y_world_inflation,1),1) inflation_drop];
results_wf_inflation=white_robust(Y_world_inflation,X_inflation);
results_rf_inflation=white_robust(Y_region_inflation,X_inflation);
results_cf_inflation=white_robust(Y_country_inflation,X_inflation);
disp('Y = world fraction; X = constant, avg. inflation');
prt(results_wf_inflation);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_wf_inflation.beta(2) results_wf_inflation.tstat(2) ...
    results_wf_inflation.nobs results_wf_inflation.rbar],...
    'Cross-section, 1951-2009','b12:e12');
disp('Y = region fraction; X = constant, avg. inflation');
prt(results_rf_inflation);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_rf_inflation.beta(2) results_rf_inflation.tstat(2) ...
    results_rf_inflation.nobs results_rf_inflation.rbar],...
    'Cross-section, 1951-2009','b23:e23');
disp('Y = country fraction; X = constant, avg. inflation');
prt(results_cf_inflation);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_cf_inflation.beta(2) results_cf_inflation.tstat(2) ...
    results_cf_inflation.nobs results_cf_inflation.rbar],...
    'Cross-section, 1951-2009','b34:e34');

% Inflation volatility

inflation_vol=xlsread('Data_country_characteristics','Full sample',...
    'x4:x67'); % 
inflation_vol_drop=inflation_vol;
world_fraction_drop=world_fraction;
region_fraction_drop=region_fraction;
country_fraction_drop=country_fraction;
Y_world_inflation_vol=world_fraction_drop;
Y_region_inflation_vol=region_fraction_drop;
Y_country_inflation_vol=country_fraction_drop;
X_inflation_vol=[ones(size(Y_world_inflation_vol,1),1) inflation_vol_drop];
results_wf_inflation_vol=white_robust(Y_world_inflation_vol,X_inflation_vol);
results_rf_inflation_vol=white_robust(Y_region_inflation_vol,X_inflation_vol);
results_cf_inflation_vol=white_robust(Y_country_inflation_vol,X_inflation_vol)
disp('Y = world fraction; X = constant, inflation volatility');
prt(results_wf_inflation_vol);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_wf_inflation_vol.beta(2) results_wf_inflation_vol.tstat(2) ...
    results_wf_inflation_vol.nobs results_wf_inflation_vol.rbar],...
    'Cross-section, 1951-2009','b13:e13');
disp('Y = region fraction; X = constant, inflation volatility');
prt(results_rf_inflation_vol);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_rf_inflation_vol.beta(2) results_rf_inflation_vol.tstat(2) ...
    results_rf_inflation_vol.nobs results_rf_inflation_vol.rbar],...
    'Cross-section, 1951-2009','b24:e24');
disp('Y = country fraction; X = constant, inflation volatility');
prt(results_cf_inflation_vol);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_cf_inflation_vol.beta(2) results_cf_inflation_vol.tstat(2) ...
    results_cf_inflation_vol.nobs results_cf_inflation_vol.rbar],...
    'Cross-section, 1951-2009','b35:e35');

% Central bank independence

cbi=xlsread('Data_country_characteristics','Full sample',...
    't4:t67'); %
missing=[3 ; 6 ; 11 ; 12 ; 13 ; 14 ; 15 ; 18 ; 24 ; 41 ; 43 ; 44 ; ...
    46 ; 51 ; 57 ; 60 ; 63];
cbi_drop=cbi;
cbi_drop(missing,:)=[];
world_fraction_drop=world_fraction;
region_fraction_drop=region_fraction;
country_fraction_drop=country_fraction;
world_fraction_drop(missing,:)=[];
region_fraction_drop(missing,:)=[];
country_fraction_drop(missing,:)=[];
Y_world_cbi=world_fraction_drop;
Y_region_cbi=region_fraction_drop;
Y_country_cbi=country_fraction_drop;
X_cbi=[ones(size(Y_world_cbi,1),1) cbi_drop];
results_wf_cbi=white_robust(Y_world_cbi,X_cbi);
results_rf_cbi=white_robust(Y_region_cbi,X_cbi);
results_cf_cbi=white_robust(Y_country_cbi,X_cbi);
disp('Y = world fraction; X = constant, central bank independence');
prt(results_wf_cbi);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_wf_cbi.beta(2) results_wf_cbi.tstat(2) ...
    results_wf_cbi.nobs results_wf_cbi.rbar],...
    'Cross-section, 1951-2009','b14:e14');
disp('Y = region fraction; X = constant, central bank independence');
prt(results_rf_cbi);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_rf_cbi.beta(2) results_rf_cbi.tstat(2) ...
    results_rf_cbi.nobs results_rf_cbi.rbar],...
    'Cross-section, 1951-2009','b25:e25');
disp('Y = country fraction; X = constant, central bank independence');
prt(results_cf_cbi);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_cf_cbi.beta(2) results_cf_cbi.tstat(2) ...
    results_cf_cbi.nobs results_cf_cbi.rbar],...
    'Cross-section, 1951-2009','b36:e36');

% Multivariate excl. central bank independence

RHS=[sw_open sw_institutions liquid gov_share log(gdp) inflation inflation_vol];
missing=[ 63];
missing=[63];
missing=[60 ; 61];

missing=[1 ; 24 ; 33 ; 35 ; 36 ; 41 ; 43 ; 46 ; 47 ; 54 ; 56 ; 57 ; 60 ; 63];
RHS_drop=RHS;
RHS_drop(missing,:)=[];
world_fraction_drop=world_fraction;
region_fraction_drop=region_fraction;
country_fraction_drop=country_fraction;
world_fraction_drop(missing,:)=[];
region_fraction_drop(missing,:)=[];
country_fraction_drop(missing,:)=[];
Y_world=world_fraction_drop;
Y_region=region_fraction_drop;
Y_country=country_fraction_drop;
X=[ones(size(Y_world,1),1) RHS_drop];
results_wf_all_ecbi=white_robust(Y_world,X);
results_rf_all_ecbi=white_robust(Y_region,X);
results_cf_all_ecbi=white_robust(Y_country,X);
disp('Y = world fraction; X = constant, all variables excl. central bank independence');
prt(results_wf_all_ecbi);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_wf_all_ecbi.beta(2:8) results_wf_all_ecbi.tstat(2:8)],...
    'Cross-section, 1951-2009','g7:h13');
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_wf_all_ecbi.nobs results_wf_all_ecbi.rbar results_wf_all_ecbi.fstat results_wf_all_ecbi.pval],...
    'Cross-section, 1951-2009','i7:l7');
disp('Y = region fraction; X = constant, all variables excl. central bank independence');
prt(results_rf_all_ecbi);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_rf_all_ecbi.beta(2:8) results_rf_all_ecbi.tstat(2:8)],...
    'Cross-section, 1951-2009','g18:h24');
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_rf_all_ecbi.nobs results_rf_all_ecbi.rbar results_rf_all_ecbi.fstat results_rf_all_ecbi.pval],...
    'Cross-section, 1951-2009','i18:l18');
disp('Y = country fraction; X = constant, all variables excl. central bank independence');
prt(results_cf_all_ecbi);
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_cf_all_ecbi.beta(2:8) results_cf_all_ecbi.tstat(2:8)],...
    'Cross-section, 1951-2009','g29:h35');
xlswrite('C:\Research\International_inflation\International_inflation_results',...
    [results_cf_all_ecbi.nobs results_cf_all_ecbi.rbar results_cf_all_ecbi.fstat results_cf_all_ecbi.pval],...
    'Cross-section, 1951-2009','i29:l29');
