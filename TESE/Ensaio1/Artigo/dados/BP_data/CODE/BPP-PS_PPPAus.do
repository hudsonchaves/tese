
use ${dir_rawdata}\ppp_daily_australia.dta, clear


*Use these for the paper
*Relative Prices and E
label var e_oficial "E"
label var e_ppp "P_AUS/P_US"
line e_ppp e_oficial date, yscale(log) title("Relative Prices and Nominal Exchange Rate") ytitle("Local Currency per US Dollar") xtitle("Date") lcolor(black gs8) lpattern(solid longdash) lwidth(medthick)

graph save ${dir_graphs}\\AUSTRALIA_RP_E, replace
graph export ${dir_graphs}\\AUSTRALIA_RP_E.png, replace
graph export ${dir_graphs}\\AUSTRALIA_RP_E.eps, replace

*RER
line rer date, title("Real Exchange Rate") ytitle("RER = (P_AUS / P_US) * (1/E) ") xtitle("Date") lcolor(black gs8) lpattern(solid longdash) lwidth(medthick)
graph save ${dir_graphs}\\AUSTRALIA_RER, replace
graph export ${dir_graphs}\\AUSTRALIA_RER.png, replace
graph export ${dir_graphs}\\AUSTRALIA_RER.eps, replace
