**This script uses the PriceStats indices as an input, creating graphs for the paper. 

*graphs for Argentina
use ${dir_rawdata}\pricestats_bpp_arg_usa.dta, clear

levelsof country, clean
global listcountry `r(levels)'

foreach country in $listcountry {

tsline annualPS annualCPI if country=="`country'",  ytitle(%)  ylabel(,format(%9.0f)) ttitle("") tlabel(, format(%tdMon-CCYY)) title(Annual Inflation Rate) 
graph export ${dir_graphs}\inflation_annual_`country'.png, replace
graph export ${dir_graphs}\inflation_annual_`country'.eps, replace

}

*Zoom in USA
tsline indexPS indexCPI if country=="USA" & date>=17623 & date<=18018, ytitle() ylabel(,format(%9.0f))  ttitle("") tlabel(, format(%tdMon-CCYY)) title(Price Index) tline(16sep2008 20dec2008, lpattern(dash) lcolor(black))  ttext(100.8 30sep2008 "09/16"  100.8 01jan2009 "12/16" , size(medsmall)) 
graph export ${dir_graphs}\inflation_index_zoom_USA.png, replace
graph export ${dir_graphs}\inflation_index_zoom_USA.eps, replace



*Now annual graphs for all countries

*Load data
use ${dir_rawdata}\pricestats_bpp_series.dta, clear

levelsof country, clean
global listcountry `r(levels)'

foreach country in $listcountry {

tsline annualPS annualCPI if country=="`country'",  ytitle(%)  ylabel(,format(%9.0f)) ttitle("") tlabel(, format(%tdMon-CCYY)) title(Annual Inflation Rate) 
graph export ${dir_graphs}\inflation_annual_`country'.png, replace
graph export ${dir_graphs}\inflation_annual_`country'.eps, replace

}




