use ${dir_rawdata}\distribution.dta, clear

line d x if id=="online" || line d x if id=="scanner"  || line d x if id=="weekly_average" , ytitle(Density) xtitle("Size of Price Change (%)") legend(on order( 1 "Online Data" 2 "Scanner Data" 3 "Weekly Average" ) rows(1)) 
graph export ${dir_graphs}\distributionchanges3_KERNELS_scannerandweekly_${sales}_usa.png, replace
graph export ${dir_graphs}\distributionchanges3_KERNELS_scannerandweekly_${sales}_usa.eps, replace
