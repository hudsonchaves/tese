**This script uses the PriceStats indices as an input, creating graphs for the paper. 

*Get PPP daily data
use ${dir_rawdata}\ppp_daily_argentina.dta, clear

*Relative Prices and E
label var e_oficial "E"
label var e_ppp "P_ARG/P_US"
label var e_blue "Black-Market E"
line e_ppp e_oficial e_blue date, yscale(log) title("Relative Prices and Nominal Exchange Rate") ytitle("Local Currency per US Dollar") xtitle("Date") legend(rows(1)) lcolor(black gs8) lpattern(solid longdash) lwidth(medthick) 
graph save ${dir_graphs}\\Argentina_RP_E, replace
graph export ${dir_graphs}\\Argentina_RP_E.png, replace
graph export ${dir_graphs}\\Argentina_RP_E.eps, replace

*RER
line rer date, title("Real Exchange Rate") ytitle("RER = (P_ARG / P_US) * (1/E) ") xtitle("Date")
graph save ${dir_graphs}\\Argentina_RER, replace
graph export ${dir_graphs}\\Argentina_RER.png, replace
graph export ${dir_graphs}\\Argentina_RER.eps, replace


