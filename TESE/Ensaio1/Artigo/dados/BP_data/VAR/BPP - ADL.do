* The following code runs ADL regressions and constructs confidence intervals upon block bootstrap samples.

clear all
set more off

global Master "${dir_main}\VAR"
global OutputBlock "${dir_main}\VAR\Stata-Output\SE-BLOCK"
global PlotsBlock "${dir_main}\VAR\Plots\SE-BLOCK"
global PlotsBlockT "${dir_main}\VAR\Plots\SE-BLOCK-T"
global lags = 6

local Code_0 "Aggregate"
local Code_100 "Food"
local Code_500 "Household"
local Code_600 "Health"
local Code_700 "Transportation"
local Code_900 "Recreation"

foreach sector of numlist 0 100 500 600 700 900 {
	cd "${Master}"
	use DATA_RR_IMPULSE_SECTOR.dta, replace

	****
	* Create three internal and temporary files as follows:
	*1. Consolidating the R bootstrap replications sample (including dates).
	*2. Storing temporarily the bootstrap sample for replication "r".
	*3. Consolidating the R bootstrap IRFs coefficients.
	cd "${OutputBlock}"
	gen date2 = mofd(date)
	tsset date2, format (%tm)
	sort date2
	rename index_USA index_USA_0
	rename cpi_all_nsa_USA cpi_0_USA
	capture preserve
	keep date2
	drop in 1
	save Block_Boot_Dataset`sector'.dta, replace
	drop _all
	save Appending-Sector`sector'.dta, replace emptyok
	* IRF Step is set to 8, so as to have 1.5 years of impulse-responses.
	set obs 10
	gen month = _n-1
	save Block_Boot_Coefficients_`sector'.dta, replace emptyok
	restore
	****

	gen x`sector'L0 = ln(index_USA_`sector') - ln(L.index_USA_`sector')
	gen y`sector'L0 = ln(cpi_`sector'_USA) - ln(L.cpi_`sector'_USA)

	*****
	* Save a MASTER and FINAL (!!) VAR output dataset per sector, storing: steps (9+1), IRFs (original regression as per below), lower and upper bands (plug in later, using block bootstrap technique to account for time dependance in residuals).
	capture preserve
	keep date2 x`sector'L0 y`sector'L0
	* As we are using data in differences, first observation is lost missing.
	drop in 1
	tsset date2
	sort date2
	quietly: var y`sector'L0, lags(1/6) exog(L(0/6).x`sector'L0)
	irf create irf_original, step(8) set(VAR_US_Original_`sector', replace) replace
	copy VAR_US_Original_`sector'.irf VAR_US_Original_`sector'.dta, replace
	use VAR_US_Original_`sector'.dta, clear
	keep step cdm stdcdm
	drop if cdm == .
	set obs `=_N+1'
	gen irf_original = cdm[_n-1]
	gen std_original = stdcdm[_n-1]
	drop cdm stdcdm
	replace step = step[_N-1]+1 in `=_N'
	rename step month
	recode irf_original std_original (mis=0)
	save Master_Block_Results_`sector'.dta, replace
	restore
	****

	* Now re-build the original series into block stacked data.
	* We lost 1 observations due to varibles in differences and now 6 more due to lags.
	* Notice obs vs obs2 (stacked set vs original set minus 1, respectively).
	forvalues i = 1/$lags {
		gen x`sector'L`i' = L`i'.x`sector'L0
		gen y`sector'L`i' = L`i'.y`sector'L0
	}	
	 foreach v of var _all {
		drop if missing(`v') 
	}

	global obs = _N
	global obs2 = $obs + $lags
	global block_length = int(($obs)^(1/3))
	global total_blocks = ceil($obs/$block_length)
	global pseudo_total_blocks = ceil($obs2/($block_length+$lags))

	egen block_id = seq(), t($obs2) b($block_length)
	keep date2 x`sector'L0-x`sector'L$lags y`sector'L0-y`sector'L$lags block_id
	save Stacked-Data-Sector`sector'.dta, replace

	* Construct the bootstrap loop (below we set R = 200 times). 
	* Within each replication r, as the total observations may not be an (exact) multiple of the block length, we over-iterate and then pare down. 
	* "$pseudo_total_blocks" guarantees the condition.

	forvalues R = 1/200 {
		* Clean up the file storing the bootstrap sample for replication "r".
		use Appending-Sector`sector'.dta, clear
		drop _all
		save Appending-Sector`sector'.dta, replace emptyok
		
		* Create the bootstrapped sample
		forvalues h = 1/$pseudo_total_blocks {
			use Stacked-Data-Sector`sector', clear
			scalar a = int(1+$total_blocks*uniform())
			keep if block_id == scalar(a)
			gen x`sector'_rep`R' = .
			gen y`sector'_rep`R' = .
			local temp_block = _N
			local temp_block_long = _N + $lags
			set obs `temp_block_long'

			forvalues k = 1/$lags {
				local u = $lags - `k' + 1
				replace x`sector'_rep`R' = x`sector'L`k'[1] in `u'
				replace y`sector'_rep`R' = y`sector'L`k'[1] in `u'
			}
			forvalues p = 1/`temp_block' {
				local l = $lags + `p'
				replace x`sector'_rep`R' = x`sector'L0[`p'] in `l'
				replace y`sector'_rep`R' = y`sector'L0[`p'] in `l'
			}

			keep x`sector'_rep`R' y`sector'_rep`R'
			save Temp-File.dta, replace

			use Appending-Sector`sector'.dta, clear
			append using Temp-File.dta
			save Appending-Sector`sector'.dta, replace
		}
		* Consolidate each bootstrap sample "r" into a unique master dataset.
		use Block_Boot_Dataset`sector'.dta, clear
		merge 1:1 _n using Appending-Sector`sector'.dta, sorted nogenerate
		keep if _n <= $obs2
		save Block_Boot_Dataset`sector'.dta, replace
		
		* For each replication, run VAR and store IRFs coefficients. Plug in our standard errors latter.
		* Notice in each replication we are over-writing previous VAR tables. This can be changed if needed.
		tsset date2
		sort date2
		quietly: var y`sector'_rep`R', lags(1/6) exog(L(0/6).x`sector'_rep`R')
		irf create irf_block_`sector', step(8) set(VAR_US_Block_`sector', replace) replace
		copy VAR_US_Block_`sector'.irf VAR_US_Block_`sector'.dta, replace
		use VAR_US_Block_`sector'.dta, clear
		keep step cdm stdcdm
		drop if cdm ==.
		set obs `=_N+1'
		gen irf_rep`R' = cdm[_n-1]
		gen std_rep`R' = stdcdm[_n-1]
		drop cdm stdcdm
		replace step = step[_N-1]+1 in `=_N'
		rename step month
		recode irf_rep`R' std_rep`R' (mis=0)
		save Temp-Block-Coef.dta, replace

		use Block_Boot_Coefficients_`sector'.dta, clear
		merge 1:1 month using Temp-Block-Coef.dta, nogenerate 
		save Block_Boot_Coefficients_`sector'.dta, replace
	}

	use Block_Boot_Coefficients_`sector'.dta, clear
	merge 1:1 month using Master_Block_Results_`sector'.dta, nogenerate
	reshape long irf_rep std_rep, i(month) j(replication)
	rename irf_rep irf
	rename std_rep std
	* Calculate the 2.5 and 97.5 percentiles for each step. Remind we are setting 9 steps (+1, t=0) so as to have 1.5 years of impulse responses.
	gen lower = .
	gen upper = .
	forvalues g = 0/9 {
		_pctile irf if month == `g', p(2.5, 97.5)
		replace lower = r(r1) if month == `g'
		replace upper = r(r2) if month == `g'
	}
	gen t_stat = (irf - irf_original)/std
	* To avoid missing values, we set variables to 0 and thus start from step 1.
	gen t_lower = 0
	gen t_upper = 0
	forvalues g = 1/9 {
		_pctile t_stat if month == `g', p(2.5, 97.5)
		replace t_lower = irf_original - (r(r2))*(std_original) if month == `g'
		replace t_upper = irf_original - (r(r1))*(std_original) if month == `g'
	}
	
	* Graph IRFs - 1 per Sector. We run this for both bootstrap-based confidence intervals options.
	keep month lower upper t_lower t_upper irf_original
	duplicates drop
	save Master_Block_Results_`sector'.dta, replace
	capture preserve
	* Option A - Percentile
	drop if month==0
	replace month=month-1
	
	twoway (line irf_original month, color(black) lpattern(solid)) || (line upper lower month, color(gs12 gs12) lpattern(dash dash)) , saving(IRF_Block_Boot_`sector', replace) xtitle("Month") ytitle("%")  ylabel(,angle(0)) xlabel(0/`=_N-1') xtick(0/`=_N-1') legend(order(1 2) lab(1 "Cumulative IRF") lab(2 "95% CI") ) 
	*graph use IRF_Block_Boot_`sector'.gph
	cd "${PlotsBlock}"
	graph export IRF_Block_Boot_`sector'_Final.png, replace
	graph export IRF_Block_Boot_`sector'_Final.eps, replace

	* Option B - T
	restore
	cd "${OutputBlock}"
	
	*Make them start at 1
	drop if month==0
	replace month=month-1

	twoway (line irf_original month,  color(black) lpattern(solid)) || (line t_upper t_lower month, color(gs12 gs12) lpattern(dash dash)) , saving(IRF_Block_Boot_T_`sector', replace) xtitle("Month") ytitle("%") ylabel(,angle(0)) xlabel(0/`=_N-1') xtick(0/`=_N-1') legend(order(1 2) lab(1 "Cumulative IRF") lab(2 "95% CI") ) 
	
		
	*graph use IRF_Block_Boot_T_`sector'.gph
	cd "${PlotsBlockT}"
	graph export IRF_Block_Boot_T_`sector'_Final.png, replace
	graph export IRF_Block_Boot_T_`sector'_Final.eps, replace

}


* Combine All Sector Block-Boot-Percentile IRFs into 1 Graph
cd "${OutputBlock}"
graph combine IRF_Block_Boot_0.gph IRF_Block_Boot_100.gph IRF_Block_Boot_500.gph IRF_Block_Boot_600.gph IRF_Block_Boot_700.gph IRF_Block_Boot_900.gph, row(2) col(3) iscale(0.4) saving(IRF_Block_Boot_All_Combined, replace)
cd "${PlotsBlock}"
graph export IRF_Block_Boot_All_Combined.png, replace
graph export IRF_Block_Boot_All_Combined.eps, replace


* Combine All Sector Block-Boot-T IRFs into 1 Graph
cd "${OutputBlock}"
graph combine IRF_Block_Boot_T_0.gph IRF_Block_Boot_T_100.gph IRF_Block_Boot_T_500.gph IRF_Block_Boot_T_600.gph IRF_Block_Boot_T_700.gph IRF_Block_Boot_T_900.gph, row(2) col(3) iscale(0.4) saving(IRF_Block_Boot_T_All_Combined, replace)
cd "${PlotsBlockT}"
graph export IRF_Block_Boot_T_All_Combined.png, replace
graph export IRF_Block_Boot_T_All_Combined.eps, replace



******************       ******************
******************       ******************
******************       ******************
******************       ******************