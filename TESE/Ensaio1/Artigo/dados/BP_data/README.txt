***************************************************
*Replication materials for JEP Paper titled "The Billion Prices Project: Using Online Data for Measurement and Research"
*Authors: Alberto Cavallo and Roberto Rigobon
*Date: 3/2016
**************************************************

Instructions:

-The replication scripts and data files are designed for STATA 14. Some user commands used in the scripts need to be installed before using "ssc install commandname" syntax. Please run the code and check for errors to identify these commands

-The main script is "main_jep.do".  This is the only script that needs to be run. All other scripts are referenced there and will run atuomatically. Graphs will be created inside the "GRAPHS" folder in both PNG and EPS formats. The Raw data used in those graphs is in "RAWDATA"

-Before running, open main_jep.do and change the line below to match the directory where you stored these files in your computer
global dir_main "X:\Dropbox (Personal)\PAPERS\BPP JEP paper\REPLICATION"


Script Files:
main_jep.do: Main scripts. Calls all other scripts in sequence. 
CODE\BPP-PS_inflation.do: Create Graphs for Figures 1, 2, 3, and 4.
CODE\BPP-PS_PPPArg.do: Creates graphs for Argentina Figure 9.
CODE\BPP-PS_PPPAus.do: Creates graphs for Australia Figure 9.
CODE\BPP-PPP_graphs.do: Builds Figure 9 (combining graphs from the previous scripts)
CODE\BPP-stickiness.do: Generates histograms in Figure 8.
VAR\BPP - ADL.do: Runs impulse response and creates Figure 7. 


Data Files and Formats:

RAWDATA\pricestats_bpp_arg_usa.dta: Daily price index, monthly inflation rate, and annual inflation rates for Argentina and the US. The source is PriceStats (www.pricestats.com). "indexPS" is the price index computed by PriceStats. "annualPS" is the annual inflation rate for indexPS. "monthlyPS" is the monthly inflation rate for indexPS. The variables indexCPI, annualCPI, and monthlyCPI are defined the same way for the official Consumer Price Index. All series are re-normalized to 100 on the first day of data. 

RAWDATA\pricestats_bpp_series.dta: Annual inflation rates for the official CPI and PriceStats inflation indices in several countries and sectors (Figure 4). "annualCPI" is the annual inflation rate in the official Consumer Price Index. "annualPS" is the annual inflation rate in the online price index computed by PriceStats. The variable "country" contains the name of the country. For sectoral series in the US, the name follows COICOP's classification codes. For example, "USA_100" means "Food and non-alcoholic beverages", "USA_600" is "Health", and so on. For China, the country is listed as "CHINA_S" because only supermarket data is used in this case. 

RAWDATA\ppp_daily_argentina.dta: Daily real and nominal exchange rates for Argentina. All nominal exchange rates are expressed in dollars per local currency. "e_official" is the official nominal exchange rate. "e_ppp" is the nominal exchange rate implied by PPP. "e_ppp_ad" is the nominal exchange rate that would yield the average historical RER observed in the data for that country. "e_blue" is the black-market exchange rate in Argentina. "rer" is the real exchange rate when using the official nominal exchange rate. "rer_blue" is the real exchange rate when using the black-market nominal exchange rate.  

RAWDATA\ppp_daily_australia.dta: Daily real and nominal exchange rates for Australia. All nominal exchange rates are expressed in dollars per local currency. "e_official" is the official nominal exchange rate. "e_ppp" is the nominal exchange rate implied by PPP. "e_ppp_ad" is the nominal exchange rate that would yield the average historical RER observed in the data for that country. "rer" is the real exchange rate when using the official nominal exchange rate.  

RAWDATA\distribution.dta: Raw data for histograms in Figure 8. It has three columns. "d" is the density, "x" is the percentage change, "id" is the source of data (online, scanner, or weekly averaged online data)

VAR\DATA_RR_IMPULSE_SECTOR.dta: Contains the official Consumer Price Indices and online price indices for the aggregate and sectors in the US for which impulse response graphs are built. See Figure 7 and the Appendix for details. 



CODE\BPP-stickiness.do
