***************************************************
*Main STATA script for JEP Paper titled "The Billion Prices Project: Using Online Data for Measurement and Research"
*Authors: Alberto Cavallo and Roberto Rigobon
*Date: 3-2016
**************************************************

clear
set more off

**************************************************
*---UPDATE THIS PART---
*Setup 
global dir_main "X:\Dropbox (Personal)\PAPERS\BPP JEP paper\REPLICATION"
global dir_rawdata "RAWDATA"
global dir_data "DATA"
global dir_code "CODE"
global dir_results "RESULTS"
global dir_graphs "GRAPHS"
global dir_tables "TABLES"
global dir_temp "TEMP"
**************************************************

*Go to the working directory
cd "${dir_main}"

*set graph scheme
set scheme s2monowhite, perm

*Create Graphs for PS Inflation Series
do "${dir_code}\BPP-PS_inflation.do"

*Create Graphs for Argentina and Australia PPP Series
do "${dir_code}\BPP-PS_PPPArg.do"
do "${dir_code}\BPP-PS_PPPAus.do"
do "${dir_code}\BPP-PPP_graphs.do"
do "${dir_code}\BPP-stickiness.do"

*Create IRFs 
do "VAR\BPP - ADL.do"
