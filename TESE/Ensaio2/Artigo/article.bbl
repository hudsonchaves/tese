\providecommand{\abntreprintinfo}[1]{%
 \citeonline{#1}}
\setlength{\labelsep}{0pt}\begin{thebibliography}{}
\providecommand{\abntrefinfo}[3]{}
\providecommand{\abntbstabout}[1]{}
\abntbstabout{v-1.9.6 }

\bibitem[Aghion e Howitt 1990]{aghion1990model}
\abntrefinfo{Aghion e Howitt}{AGHION; HOWITT}{1990}
{AGHION, P.; HOWITT, P. \emph{A model of growth through creative destruction}.
Cambridge, 1990.}

\bibitem[An e Schorfheide 2007]{an2007bayesian}
\abntrefinfo{An e Schorfheide}{AN; SCHORFHEIDE}{2007}
{AN, S.; SCHORFHEIDE, F. Bayesian analysis of dsge models.
\emph{Econometric reviews}, Taylor \& Francis, New York, v.~26, n.~2-4, p.
  113--172, 2007.}

\bibitem[Arulampalam et al. 2002]{arulampalam2002tutorial}
\abntrefinfo{Arulampalam et al.}{ARULAMPALAM et al.}{2002}
{ARULAMPALAM, M.~S. et al. A tutorial on particle filters for online
  nonlinear/non-gaussian bayesian tracking.
\emph{Signal Processing, IEEE Transactions on}, IEEE, v.~50, n.~2, p. 174--188,
  2002.}

\bibitem[Baumol 1952]{baumol1952transactions}
\abntrefinfo{Baumol}{BAUMOL}{1952}
{BAUMOL, W.~J. The transactions demand for cash: An inventory theoretic
  approach.
\emph{The Quarterly Journal of Economics}, JSTOR, Cambridge, p. 545--556,
  1952.}

\bibitem[Caetano e Moura 2014]{caetanoa2014modelo}
\abntrefinfo{Caetano e Moura}{CAETANO; MOURA}{2014}
{CAETANO, S.~M.; MOURA, G.~V. Um modelo macroecon{\^o}mico h{\i}brido para o
  brasil: um mix de modelos dsge e var.
Foz, 2014.}

\bibitem[Castro et al. 2011]{de2011samba}
\abntrefinfo{Castro et al.}{CASTRO et al.}{2011}
{CASTRO, M.~R. D. et al. Samba: Stochastic analytical model with a bayesian
  approach.
\emph{Brazilian Review of Econometrics}, Rio de Janeiro, v.~99, n.~99, 2011.}

\bibitem[Christiano, Eichenbaum e Evans 2005]{christiano2005nominal}
\abntrefinfo{Christiano, Eichenbaum e Evans}{CHRISTIANO; EICHENBAUM;
  EVANS}{2005}
{CHRISTIANO, L.~J.; EICHENBAUM, M.; EVANS, C.~L. Nominal rigidities and the
  dynamic effects of a shock to monetary policy.
\emph{Journal of political Economy}, JSTOR, v.~113, n.~1, p. 1--45, 2005.}

\bibitem[Christoffel, Coenen e Warne 2008]{christoffel2008new}
\abntrefinfo{Christoffel, Coenen e Warne}{CHRISTOFFEL; COENEN; WARNE}{2008}
{CHRISTOFFEL, K.; COENEN, G.; WARNE, A. The new area-wide model of the euro
  area.
\emph{ECB working}, 2008.}

\bibitem[Fern{\'a}ndez-Villaverde 2010]{fernandez2010econometrics}
\abntrefinfo{Fern{\'a}ndez-Villaverde}{FERN{\'A}NDEZ-VILLAVERDE}{2010}
{FERN{\'A}NDEZ-VILLAVERDE, J. The econometrics of dsge models.
\emph{SERIEs}, Springer, v.~1, n.~1-2, p. 3--49, 2010.}

\bibitem[Fern{\'a}ndez-Villaverde, Ram{\'\i}rez e Schorfheide
  2016]{fernandez2016solution}
\abntrefinfo{Fern{\'a}ndez-Villaverde, Ram{\'\i}rez e
  Schorfheide}{FERN{\'A}NDEZ-VILLAVERDE; RAM{\'I}REZ; SCHORFHEIDE}{2016}
{FERN{\'A}NDEZ-VILLAVERDE, J.; RAM{\'I}REZ, J. F.~R.; SCHORFHEIDE, F.
  \emph{Solution and Estimation Methods for DSGE Models}.
Cambridge, 2016.}

\bibitem[Ferreira 2015]{ferreira2015regra}
\abntrefinfo{Ferreira}{FERREIRA}{2015}
{FERREIRA, D. Regra de taylor e pol{\'\i}tica monet{\'a}ria no brasil:
  considera{\c{c}}{\~o}es emp{\'\i}ricas a partir de um modelo dsge para uma
  pequena economia aberta.
\emph{Revista Teoria e Evid{\^e}ncia Econ{\^o}mica}, Passo Fundo, v.~21, n.~44,
  2015.}

\bibitem[Florian e Montoro 2009]{florian2009development}
\abntrefinfo{Florian e Montoro}{FLORIAN; MONTORO}{2009}
{FLORIAN, D.; MONTORO, C. Development of mega-d: A dsge model for policy
  analysis.
\emph{Central Reserve Bank of Peru}, 2009.}

\bibitem[Furlani, Portugal e Laurini 2010]{furlani2010exchange}
\abntrefinfo{Furlani, Portugal e Laurini}{FURLANI; PORTUGAL; LAURINI}{2010}
{FURLANI, L. G.~C.; PORTUGAL, M.~S.; LAURINI, M.~P. Exchange rate movements and
  monetary policy in brazil: Econometric and simulation evidence.
\emph{Economic Modelling}, Elsevier, Surrey, v.~27, n.~1, p. 284--295, 2010.}

\bibitem[Gali e Monacelli 2005]{gali2005monetary}
\abntrefinfo{Gali e Monacelli}{GALI; MONACELLI}{2005}
{GALI, J.; MONACELLI, T. Monetary policy and exchange rate volatility in a
  small open economy.
\emph{The Review of Economic Studies}, Oxford University Press, v.~72, n.~3, p.
  707--734, 2005.}

\bibitem[Gertler, Gilchrist e Natalucci 2007]{gertler2007external}
\abntrefinfo{Gertler, Gilchrist e Natalucci}{GERTLER; GILCHRIST;
  NATALUCCI}{2007}
{GERTLER, M.; GILCHRIST, S.; NATALUCCI, F.~M. External constraints on monetary
  policy and the financial accelerator.
\emph{Journal of Money, Credit and Banking}, Wiley Online Library, Columbus,
  v.~39, n.~2-3, p. 295--330, 2007.}

\bibitem[Gonz{\'a}lez et al. 2011]{gonzalez2011policy}
\abntrefinfo{Gonz{\'a}lez et al.}{GONZ{\'A}LEZ et al.}{2011}
{GONZ{\'A}LEZ, A. et al. Policy analysis tool applied to colombian needs:
  Patacon model description.
\emph{Ensayos sobre Pol{\'\i}tica Econ{\'o}mica}, Banco de la Rep{\'u}blica,
  Colombia, v.~29, n.~66, p. 222--245, 2011.}

\bibitem[Grossman e Helpman 1993]{grossman1993innovation}
\abntrefinfo{Grossman e Helpman}{GROSSMAN; HELPMAN}{1993}
{GROSSMAN, G.~M.; HELPMAN, E. \emph{Innovation and growth in the global
  economy}. [S.l.]: MIT press, 1993.}

\bibitem[Harrison et al. 2005]{harrison2005bank}
\abntrefinfo{Harrison et al.}{HARRISON et al.}{2005}
{HARRISON, R. et al. \emph{The Bank of England quarterly model}. [S.l.]: Bank
  of England London, 2005.}

\bibitem[Hodge et al. 2008]{hodge2008small}
\abntrefinfo{Hodge et al.}{HODGE et al.}{2008}
{HODGE, A. et al. \emph{A small BVAR-DSGE model for forecasting the Australian
  economy}. Ankara: Economic Research Department, Reserve Bank of Australia,
  2008.}

\bibitem[Hodrick e Prescott 1997]{hodrick1997postwar}
\abntrefinfo{Hodrick e Prescott}{HODRICK; PRESCOTT}{1997}
{HODRICK, R.~J.; PRESCOTT, E.~C. Postwar us business cycles: an empirical
  investigation.
\emph{Journal of Money, credit, and Banking}, JSTOR, p. 1--16, 1997.}

\bibitem[Ireland 2004]{ireland2004method}
\abntrefinfo{Ireland}{IRELAND}{2004a}
{IRELAND, P.~N. A method for taking models to the data.
\emph{Journal of Economic Dynamics and Control}, Elsevier, v.~28, n.~6, p.
  1205--1226, 2004.}

\bibitem[Ireland 2004]{ireland2004technology}
\abntrefinfo{Ireland}{IRELAND}{2004b}
{IRELAND, P.~N. Technology shocks in the new keynesian model.
\emph{Review of Economics and Statistics}, MIT Press, v.~86, n.~4, p. 923--936,
  2004.}

\bibitem[Kam, Lees e Liu 2009]{kam2009uncovering}
\abntrefinfo{Kam, Lees e Liu}{KAM; LEES; LIU}{2009}
{KAM, T.; LEES, K.; LIU, P. Uncovering the hit list for small inflation
  targeters: A bayesian structural analysis.
\emph{Journal of Money, Credit and Banking}, Wiley Online Library, Columbus,
  v.~41, n.~4, p. 583--618, 2009.}

\bibitem[Keynes 1924]{keynes1924tract}
\abntrefinfo{Keynes}{KEYNES}{1924}
{KEYNES, J.~M. A tract on monetary reform.
Macmillan and co, 1924.}

\bibitem[Lees et al. 2009]{lees2009introducing}
\abntrefinfo{Lees et al.}{LEES et al.}{2009}
{LEES, K. et al. Introducing kitt: The reserve bank of new zealand new dsge
  model for forecasting and policy design.
\emph{Reserve Bank of New Zealand Bulletin}, Reserve Bank of New Zealand,
  v.~72, n.~2, p. 5--20, 2009.}

\bibitem[Linardi et al. 2016]{de2016assessing}
\abntrefinfo{Linardi et al.}{LINARDI et al.}{2016}
{LINARDI, F. de M. et al. \emph{Assessing the Fit of a Small Open-Economy DSGE
  Model for the Brazilian Economy}.
Brasilia, 2016.}

\bibitem[Lubik e Schorfheide 2005]{lubik2005ka}
\abntrefinfo{Lubik e Schorfheide}{LUBIK; SCHORFHEIDE}{2005}
{LUBIK, T.~A.; SCHORFHEIDE, F. \emph{A Bayesian Look at New Open Economy
  Macroeconomics}. [S.l.]: Cambridge: MIT Press, 2005.}

\bibitem[Lubik e Schorfheide 2007]{lubik2007central}
\abntrefinfo{Lubik e Schorfheide}{LUBIK; SCHORFHEIDE}{2007}
{LUBIK, T.~A.; SCHORFHEIDE, F. Do central banks respond to exchange rate
  movements? a structural investigation.
\emph{Journal of Monetary Economics}, Elsevier, Amsterdam, v.~54, n.~4, p.
  1069--1087, 2007.}

\bibitem[Lucas 1976]{lucas1976econometric}
\abntrefinfo{Lucas}{LUCAS}{1976}
{LUCAS, R.~E. Econometric policy evaluation: A critique. In:  NORTH-HOLLAND.
  \emph{Carnegie-Rochester conference series on public policy}. [S.l.], 1976.
  v.~1, p. 19--46.}

\bibitem[Lucas 1988]{lucas1988mechanics}
\abntrefinfo{Lucas}{LUCAS}{1988}
{LUCAS, R.~E. On the mechanics of economic development.
\emph{Journal of monetary economics}, Elsevier, Amsterdam, v.~22, n.~1, p.
  3--42, 1988.}

\bibitem[Medina, Soto et al. 2007]{medina2007chilean}
\abntrefinfo{Medina, Soto et al.}{MEDINA; SOTO et al.}{2007}
{MEDINA, J.~P.; SOTO, C. et al. The chilean business cycles through the lens of
  a stochastic general equilibrium model.
\emph{Central Bank of Chile Working Papers}, Santiago, v.~457, 2007.}

\bibitem[Murchison, Rennison et al. 2006]{murchison2006totem}
\abntrefinfo{Murchison, Rennison et al.}{MURCHISON; RENNISON et al.}{2006}
{MURCHISON, S.; RENNISON, A. et al. \emph{ToTEM: The Bank of Canada's New
  Quarterly Projection Model}.
[S.l.], 2006.}

\bibitem[Negro e Schorfheide 2004]{del2004priors}
\abntrefinfo{Negro e Schorfheide}{NEGRO; SCHORFHEIDE}{2004}
{NEGRO, M. D.; SCHORFHEIDE, F. Priors from general equilibrium models for vars.
\emph{International Economic Review}, Wiley Online Library, v.~45, n.~2, p.
  643--673, 2004.}

\bibitem[Negro e Schorfheide 2006]{del2006good}
\abntrefinfo{Negro e Schorfheide}{NEGRO; SCHORFHEIDE}{2006}
{NEGRO, M. D.; SCHORFHEIDE, F. How good is what you've got? dgse-var as a
  toolkit for evaluating dsge models.
\emph{Economic Review-Federal Reserve Bank of Atlanta}, Federal Reserve Bank of
  Atlanta, v.~91, n.~2, p.~21, 2006.}

\bibitem[Negro e Schorfheide 2007]{del2007monetary}
\abntrefinfo{Negro e Schorfheide}{NEGRO; SCHORFHEIDE}{2007}
{NEGRO, M. D.; SCHORFHEIDE, F. \emph{Monetary policy analysis with potentially
  misspecified models}.
Cambridge, 2007.}

\bibitem[Negro e Schorfheide 2008]{delinflation}
\abntrefinfo{Negro e Schorfheide}{NEGRO; SCHORFHEIDE}{2008}
{NEGRO, M. D.; SCHORFHEIDE, F. Inflation dynamics in a small open economy model
  under inflation targeting.
New York, 2008.}

\bibitem[Negro et al. 2007]{del2007fit}
\abntrefinfo{Negro et al.}{NEGRO et al.}{2007}
{NEGRO, M. D. et al. On the fit of new keynesian models.
\emph{Journal of Business \& Economic Statistics}, Taylor \& Francis,
  Washington, v.~25, n.~2, p. 123--143, 2007.}

\bibitem[Nunes 2015]{nunes2015tres}
\abntrefinfo{Nunes}{NUNES}{2015}
{NUNES, A. F. N.~d. Tr{\^e}s ensaios sobre intermedia{\c{c}}{\~a}o financeira
  em modelos dsge aplicados ao brasil.
Porto Alegre, 2015.}

\bibitem[Palma 2012]{palma2012ensaios}
\abntrefinfo{Palma}{PALMA}{2012}
{PALMA, A.~A. Ensaios sobre pol{\'\i}tica monet{\'a}ria no brasil:
  prefer{\^e}ncias do banco central e taxa natural de juros.
Porto Alegre, 2012.}

\bibitem[Ramsey 1928]{ramsey1928mathematical}
\abntrefinfo{Ramsey}{RAMSEY}{1928}
{RAMSEY, F.~P. A mathematical theory of saving.
\emph{The economic journal}, JSTOR, v.~38, n.~152, p. 543--559, 1928.}

\bibitem[Rebelo 1990]{rebelo1990long}
\abntrefinfo{Rebelo}{REBELO}{1990}
{REBELO, S.~T. \emph{Long run policy analysis and long run growth}.
Cambridge, 1990.}

\bibitem[Romer 1996]{romer1996advanced}
\abntrefinfo{Romer}{ROMER}{1996}
{ROMER, D. \emph{Advanced macroeconomics}. [S.l.]: mcgraw-hill companies,
  1996.}

\bibitem[Romer 1989]{romer1989endogenous}
\abntrefinfo{Romer}{ROMER}{1989}
{ROMER, P. \emph{Endogenous technological change}.
Cambridge, 1989.}

\bibitem[Romer 1986]{romer_1986increasing}
\abntrefinfo{Romer}{ROMER}{1986}
{ROMER, P.~M. Increasing returns and long run growth journal of political
  economv.
\emph{Vol94, pp1002-1037}, 1986.}

\bibitem[Romer 1987]{romer1987crazy}
\abntrefinfo{Romer}{ROMER}{1987}
{ROMER, P.~M. Crazy explanations for the productivity slowdown. In:  \emph{NBER
  Macroeconomics Annual 1987, Volume 2}. [S.l.]: The MIT Press, 1987. p.
  163--210.}

\bibitem[Samuelson 1958]{samuelson1958exact}
\abntrefinfo{Samuelson}{SAMUELSON}{1958}
{SAMUELSON, P.~A. An exact consumption-loan model of interest with or without
  the social contrivance of money.
\emph{The journal of political economy}, JSTOR, p. 467--482, 1958.}

\bibitem[Schorfheide 2011]{schorfheide2011estimation}
\abntrefinfo{Schorfheide}{SCHORFHEIDE}{2011}
{SCHORFHEIDE, F. \emph{Estimation and evaluation of DSGE models: progress and
  challenges}.
Cambridge, 2011.}

\bibitem[Smets e Wouters 2002]{smets2002openness}
\abntrefinfo{Smets e Wouters}{SMETS; WOUTERS}{2002}
{SMETS, F.; WOUTERS, R. Openness, imperfect exchange rate pass-through and
  monetary policy.
\emph{Journal of monetary Economics}, Elsevier, Amsterdam, v.~49, n.~5, p.
  947--981, 2002.}

\bibitem[Smets e Wouters 2003]{smets2003estimated}
\abntrefinfo{Smets e Wouters}{SMETS; WOUTERS}{2003}
{SMETS, F.; WOUTERS, R. An estimated dynamic stochastic general equilibrium
  model of the euro area.
\emph{Journal of the European economic association}, Wiley Online Library,
  v.~1, n.~5, p. 1123--1175, 2003.}

\bibitem[Solow 1956]{solow1956contribution}
\abntrefinfo{Solow}{SOLOW}{1956}
{SOLOW, R.~M. A contribution to the theory of economic growth.
\emph{The quarterly journal of economics}, JSTOR, Cambridge, p. 65--94, 1956.}

\bibitem[Souza et al. 2016]{souza2016efeitos}
\abntrefinfo{Souza et al.}{SOUZA et al.}{2016}
{SOUZA, E. T. d.~C. et al. Os efeitos da intera{\c{c}}{\~a}o entre as
  pol{\'\i}ticas fiscal e monet{\'a}ria sobre vari{\'a}veis macroeconomicas da
  economia brasileira.
Universidade Federal de Juiz de Fora (UFJF), Juiz de Fora, 2016.}

\bibitem[Stock e Watson 2001]{stock2001vector}
\abntrefinfo{Stock e Watson}{STOCK; WATSON}{2001}
{STOCK, J.~H.; WATSON, M.~W. Vector autoregressions.
\emph{The Journal of Economic Perspectives}, JSTOR, Nashville, v.~15, n.~4, p.
  101--115, 2001.}

\bibitem[Tobin 1956]{tobin1956interest}
\abntrefinfo{Tobin}{TOBIN}{1956}
{TOBIN, J. The interest-elasticity of transactions demand for cash.
\emph{The Review of Economics and Statistics}, JSTOR, p. 241--247, 1956.}

\bibitem[Uhlig 1998]{uhlig1998toolkit}
\abntrefinfo{Uhlig}{UHLIG}{1998}
{UHLIG, H. A toolkit for analysing nonlinear dynamic stochastic models easily.
\emph{QM\&RBC Codes}, Quantitative Macroeconomics \& Real Business Cycles,
  1998.}

\end{thebibliography}
