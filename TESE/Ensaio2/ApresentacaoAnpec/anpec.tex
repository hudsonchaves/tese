\documentclass[9pt]{beamer}

%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%      TEMA      %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usetheme{Madrid}                         % tema
\usecolortheme{orchid}                    % cores
\usefonttheme[onlymath]{serif}            % fonte modo matematico

%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%    PACOTES      %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[alf]{abntex2cite}             % citações no padrão ABNT
\usepackage[brazil]{babel}                % idioma
\usepackage{Sweave}                       % Sweave
\usepackage{color}                        % controle das cores
\usepackage[T1]{fontenc}                  % codificacao de fontes
\usepackage{graphicx}                     % inclusão de gráficos
\usepackage[utf8]{inputenc}               % codificacao de caracteres
\usepackage{lmodern}
\usepackage{array}
\usepackage{multirow}
\usepackage{enumerate}                    % índices numéricos 
\usepackage{footnote}                     % Lidar com notas de rodapé
\usepackage{booktabs}                     % Tabelas com qualidade de publicação diversas situações
\usepackage{hyperref}                     % para citar hyperlinks da web
\usepackage{adjustbox}
\usepackage{caption}
\newcommand\fnote[1]{\captionsetup{font=small}\caption*{#1}}

%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%   INF. DOCUMENTO    %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title[45º Encontro Nacional de Economia]{Dinâmica da inflação para uma economia sob metas de inflação: Evidências para o Brasil usando modelos DSGE-VAR}
\author[ANPEC]{Hudson Costa \textsuperscript{1} \and Sabino Porto \inst{2} \and Helberte Almeida \inst{3}}
\institute[]{\textsuperscript{1} Appus | HR Analytics \and \inst{2} UFRGS \and \inst{3} UFSC}

\date{}

\begin{document}
\input{anpec-concordance}

%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%     SUMÁRIO     %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
   \titlepage
\end{frame}

\begin{frame}[plain]\frametitle{Sumário}
\small\tableofcontents
\end{frame}

%%%
% INTRODUÇÃO/MOTIVAÇÃO
%%%

\subsection{Introdução/Motivação}

\begin{frame}\frametitle{Introdução/Motivação}
  \begin{itemize}
    \item Desde janeiro de 1999, o Brasil opera no regime de câmbio flutuante rompendo com décadas de controle;
    \item Neste período, especificamente no contexto externo, o regime de câmbio flutuante enfrentou várias adversidades:
    \begin{itemize}
      \item Colapso dos preços das ações de empresas de alta tecnologia em 2000;
      \item Crise da Argentina em 2001;
      \item Ataques terroristas de 11 de setembro de 2001;
      \item Crise de confiança em 2002;
      \item Colapso do banco Lehman Brothers em 2008;
      \item Mais recente da dívida soberana europeia e a crise política atual
    \end{itemize}
    \item Neste cenário, podemos afirmar que a autoridade monetária responde aos movimentos do câmbio para alcançar a meta de inflação?
  \end{itemize}
\end{frame}


\begin{frame}\frametitle{Introdução/Motivação}
  \begin{itemize}
    \item Modelos DSGE são confeccionados a partir de exercícios de maximização de utilidade de cada um dos envolvidos (familias, firmas, governo) partindo de expectativas racionais;
    \item Desde o seu surgimento, BACENs têm construído e estimado modelos DSGE com rigidez nominal (SAMBA no Brasil);
    \item Contudo, tais modelos enfrentam desafios importantes, Stock e Watson (2001), Ireland (2004b) e Schorfheide (2011):
    \begin{itemize}
      \item Fragilidade das estimativas dos parâmetros;
      \item Ajuste estatístico;
      \item Fraca confiabilidade das previsões. 
    \end{itemize}
    \item Principalmente em função das \textbf{restrições sobre os parâmetros a serem estimados} (microfundamentações)
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Introdução/Motivação}
  \begin{itemize}
  \item Objetivos deste estudo:
    \begin{itemize}
      \item Aplicar a abordagem DSGE-VAR na discussão sobre a reação da autoridade monetária às oscilações na taxa de câmbio;
      \item Estimar uma regra de política monetária para o Brasil;
      \item Estudar o grau de má especificação do modelo DSGE proposto (comparar com DSGE-VAR);
      \item Examinar se o BACEN conseguiu isolar a inflação de choques externos.
    \end{itemize}
  \item Alguns resultados e conclusões:
      \begin{itemize}
          \item As respostas aos desvios da taxa de câmbio são diferentes de zero e menores do que as respostas aos desvios da inflação;
          \item O ajuste do modelo DSGE é consideravelmente pior do que o ajuste do modelo DSGE-VAR o que indica que de um ponto de vista estatístico existem evidências de que as restrições impostas do modelo teórico são violadas nos dados.
      \end{itemize}
  \end{itemize}
\end{frame}

%%%
% METODOLOGIA
%%%

\subsection{Metodologia}

\begin{frame}\frametitle{Metodologia - Modelo Teórico}

Neste estudo, utiliza-se como arcabouço o modelo de \citeonline{lubik2007central}, para o qual uma primeira versão é apresentada por \citeonline{gali2005monetary}.

\begin{equation}\label{eq20en01}
{\tilde {y}}_{t}={E}_{t}{\tilde {y}}_{t+1}-\chi \left({\tilde {R}}_{t}-{E}_{t}{\tilde {\pi}}_{t+1} \right) -{\rho}_{z}{\tilde {z}}_{t}-\alpha \chi {E}_{t}\Delta {\tilde {q}}_{t+1}+\alpha \left(2-\alpha  \right) \frac{1-\tau}{\tau}{E}_{t}{\Delta \tilde{y}}_{t+1}^{*}
\end{equation}

\begin{equation}\label{eq21en01}
{\tilde {\pi}}_{t}={\beta E}_{t}{\tilde {\pi}}_{t+1}+\alpha {\beta E}_{t}\Delta {\tilde {q}}_{t+1}-\alpha \Delta {\tilde {q}}_{t}+\frac {\kappa}{\chi} \left({\tilde {y}}_{t}-{\tilde{\bar {y}}}_{t} \right) 
\end{equation}

\begin{equation}\label{eq22en01}
{\tilde{R}}_{t}={{\rho}_{R}{\tilde {R}}_{t-1}+\left( 1-{\rho}_{R} \right) \left[ {\Psi}_{\pi}{\tilde {\pi}}_{t}+{\Psi}_{y}{\tilde {y}}_{t}+{\Psi}_{\Delta e}{\Delta \tilde{e}}_{t} \right] +{\varepsilon}_{t}^{R}}
\end{equation}

\begin{equation}\label{eq23en01}
{A}_{t}={A}_{t-1}+{\varepsilon}_{z,t}
\end{equation}

\end{frame}

\begin{frame}\frametitle{Metodologia - Modelo Teórico}
\begin{equation}\label{eq24en01}
\Delta{\tilde {q}}_{t}={\rho}_{q}\Delta {\tilde {q}}_{t-1}+{\varepsilon}_{q,t}
\end{equation}

\begin{equation}\label{eq25en01}
{\tilde{y}}_{t}^{*}={\rho}_{{y}^{*}}{\tilde {y}}_{t-1}^{*}+{\varepsilon}_{{y}_{t}^{*}}
\end{equation}

\begin{equation}\label{eq26en01}
{\tilde{\pi}}_{t}^{*}={\rho}_{{\pi}^{*}}{\tilde {\pi}}_{t-1}^{*}+{\varepsilon}_{{\pi}_{t}^{*}}
\end{equation}

\begin{equation}\label{eq27en01}
\Delta{\tilde{e}}_{t}={\tilde {\pi}}_{t}-\left(1-\alpha  \right) \Delta {\tilde {q}}_{t}-{\tilde {\pi}}_{t}^{*}
\end{equation}

\noindent onde as variáveis observáveis são: produto \textbf{($y_{t}$)}, inflação \textbf{($\pi_{t}$)}, taxa de juros nominal \textbf{($R_{t}$)}, termos de troca \textbf{($q_{t}$)} e taxa nominal de câmbio \textbf{(${e}_{t}$)}.

\end{frame}

\begin{frame}\frametitle{Metodologia - Modelo Teórico}
\begin{table}[h!]
  \tiny
  \centering
\caption{Parâmetros do modelo}
\begin{tabular}{cc}
\hline
\textbf{Parâmetro}                       & \textbf{Descrição}                                                                            \\ \hline
\multicolumn{2}{c}{Famílias e Firmas}                                                                                                                                                               \\ \hline
$\tau$                                   & \multicolumn{1}{l}{Elasticidade de substibuição intertemporal}           \\
$\alpha$                                 & \multicolumn{1}{l}{Proporção de importações no consumo doméstico}        \\
${r}^{SS}$                               & \multicolumn{1}{l}{Taxa de juros de estado estacionário}                 \\ \hline
\multicolumn{2}{c}{Curva de Phillips}                                                                                                                                                               \\ \hline
$\kappa$                                 & \multicolumn{1}{l}{Determina, junto com $\alpha$ e $\tau$, a inclinação} \\ \hline
\multicolumn{2}{c}{Regra de Taylor doméstica}                                                                                                                                                                 \\ \hline
$\rho_{R}$                               & \multicolumn{1}{l}{Parâmetro de suavização da taxa de juros}   \\
$\psi_{\pi}$                             & \multicolumn{1}{l}{Coeficiente da inflação} \\
$\psi_{y}$                               & \multicolumn{1}{l}{Coeficiente do produto}  \\ 
$\psi_{\Delta \epsilon}$                 & \multicolumn{1}{l}{Coeficiente do câmbio}   \\ \hline
\multicolumn{2}{c}{Persistência dos choques exógenos}                                                                                                                                               \\ \hline
$\rho_{q}$                               & \multicolumn{1}{l}{Persistência do choque nos termos de troca}           \\
$\rho_{z}$                               & \multicolumn{1}{l}{Persistência do choque na tecnologia}                 \\
$\rho_{{y}^{*}}$                         & \multicolumn{1}{l}{Persistência do choque no produto externo}            \\
$\rho_{{\pi}^{*}}$                       & \multicolumn{1}{l}{Persistência do choque na inflação externa}           \\ \hline
\multicolumn{2}{c}{Desvio Padrão dos choques exógenos}                                                                                                                                              \\ \hline
$\sigma_{R}$                             & \multicolumn{1}{l}{Desvio-padrão do choque na taxa de juros}             \\
$\sigma_{q}$                             & \multicolumn{1}{l}{Desvio-padrão do choque nos termos de troca}          \\
$\sigma_{z}$                             & \multicolumn{1}{l}{Desvio-padrão do choque na tecnologia}                \\
$\sigma_{{y}^{*}}$                       & \multicolumn{1}{l}{Desvio-padrão do choque no produto externo}           \\
$\sigma_{{\pi}^{*}}$                     & \multicolumn{1}{l}{Desvio-padrão do choque na inflação externa}          \\ \hline
\end{tabular}
\label{table:01en01}
\end{table}

\end{frame}

\begin{frame}\frametitle{Metodologia - DSGE-VAR}
Suponha que temos o seguinte modelo VAR:

\begin{equation}\label{eq07en01}
{y}_{t}={\Phi}_{0}+{\Phi}_{1}{y}_{t-1}+...+{\Phi}_{p}{y}_{t-p}+{u}_{t}
\end{equation}

\noindent onde ${y}_{t}$ é um vetor $n \times 1$ de variáveis no tempo $t$, ${\Phi}_{i}$ são os coeficientes para $i=0,1,...,p$ e ${u}_{t}\sim N\left(0,{\Sigma}_{u} \right)$. Tal como um sistema, pode ser representado mais parcimoniosamente como:

\begin{equation}\label{eq08en01}
Y=X\Phi +U
\end{equation}

Condicional à alguns valores inicias, a função de verossimilhança para estes dados amostrais é:

\begin{equation}\label{eq09en01}
P\left(Y|\Phi ,{\Sigma}_{u} \right) \propto {\left|{\Sigma}_{u} \right|}^{-{T}/{2}}exp\left(-\frac{1}{2} tr\left[ {\Sigma}_{u}^{-1}{\left(Y-X\Phi  \right)}^{'}\left(Y-X\Phi  \right)  \right]  \right)
\end{equation}
\end{frame}

\begin{frame}\frametitle{Metodologia - DSGE-VAR}
Suponha que $\lambda T$ observações artificiais são geradas (sobrescrito $*$). \citeonline{del2004priors} mostraram que a verossimilhança desta amostra artificial é:

\begin{equation}\label{eq10en01}
P({Y(\theta)}^{*}|\Phi,{\Sigma}_{u})\propto{|{\Sigma}_{u}|}^{-{\lambda T}/{2}}exp(-\frac{1}{2} tr[{\Sigma}_{u}^{-1}{({Y}^{*}-{X}^{*}\Phi)}^{'}({Y}^{*}-{X}^{*}\Phi)])
\end{equation}

A verossimilhança conjunta da amostra dos dados atuais e artificiais é, então:

\begin{equation}\label{eq11en01}
P({Y(\theta)}^{*},Y|\Phi,{\Sigma}_{u})\propto P(Y|\Phi,{\Sigma}_{u})P({Y(\theta)}^{*}|\Phi,{\Sigma}_{u})
\end{equation}

Implicitamente estamos condicionados à escolha do hiperparâmetro $\lambda$ que maximiza a densidade marginal dos dados:

\begin{equation}\label{eq19en01}
\max _{\lambda}{{P}_{\lambda}(Y)} =\quad \int {P(Y|\Phi,{\Sigma}_{u})}{P}_{\lambda}(\Phi,{\Sigma}_{u}|\theta) P(\theta) d\theta 
\end{equation}
\end{frame}

\begin{frame}\frametitle{Metodologia - DSGE-VAR}
  \begin{figure}[h]
  \caption{Verossmilhança Marginal e prioris DSGE}
  \centering
    \includegraphics[width=0.8\linewidth]{Figure/dsge_dsgevar1.png}
  \fnote{\textbf{Fonte:} Adaptado de \citeonline{del2006good}.}
  \end{figure}
\end{frame}

\begin{frame}\frametitle{Metodologia - DSGE-VAR}
  \begin{figure}[h]
  \caption{Verossmilhança Marginal sobre diferentes restrições}
  \centering
    \includegraphics[width=0.8\linewidth]{Figure/dsge_dsgevar2.png}
  \fnote{\textbf{Fonte:} Adaptado de \citeonline{del2006good}.}
  \end{figure}
\end{frame}

\begin{frame}\frametitle{Metodologia - DSGE-VAR}
O algoritmo de Del Negro e Schrfheide (2004):
  \begin{itemize}
    \item Especificar a priori para os parâmetros do modelo DSGE (média e variância);
    \item Representar o modelo em espaço de estados, ligando o modelo teórico à equação de observações;
    \item Estimar os parâmetros do modelo DSGE com maior probabilidade a posterior;
    \item Usar o algoritmo Metropolis-Hastings para explorar a distribuição a posterior dos parâmetros do DSGE;
    \item Determinar a distribuição a posterior dos parâmetros do VAR;
    \item Obter os parâmetros do VAR;
    \item Buscar sobre o grid de valores para $\lambda$ o valor ótimo que maximiza a densidade marginal dos dados;
    \item Examinar as propriedades do DSGE-VAR (funções de resposta ao impulso)
  \end{itemize}
\end{frame}


%%%
% RESULTADOS
%%%

\subsection{Resultados}

\begin{frame}\frametitle{Resultados - Ajuste para o modelo DSGE}

\begin{itemize}
  \item se $\lambda=\infty$ as restrições são rigorosamente aplicadas;
  \item se $\lambda=0$ as restrições são completamente ignoradas na estimação do VAR
\end{itemize}

\begin{table}[h!]
  \tiny
  \centering
\caption{Ajuste para o modelo DSGE}
\begin{tabular}{lccccc}
\hline
\multicolumn{1}{c}{Especificação} & Lambda               & Log Ver. Marginal            & Posterior odds             & Log Ver. Marginal            & Posterior odds  \\ \hline
\multicolumn{1}{c}{DSGE}          & \multicolumn{1}{l}{} & -800.53                      &                            & -800.53                      &                 \\ \hline
\multicolumn{1}{c}{DSGE-VAR}      & \multicolumn{1}{l}{} & \multicolumn{2}{c}{2 Defasagens}                          & \multicolumn{2}{c}{3 Defasagens}               \\
                                  & 0.3                  & -600.30 (*)                  &   (1.000)                  & -605.00                      & (0.300)         \\
                                  & 0.4                  & -601.60                      &   (0.756)                  & -592,30                      & (0.756)         \\
                                  & 0.5                  & -604.60                      &   (0.610)                  & -590.7 (*)                   & (1.000)         \\
                                  & 0.6                  & -608.80                      &   (0.535)                  & -592.10                      & (0.835)         \\
                                  & 0.8                  & -616.90                      &   (0.408)                  & -597.20                      & (0.310)         \\
                                  & 1.0                  & -624.60                      &   (0.260)                  & -603.20                      & (0.280)         \\
                                  & 1.2                  & -631.30                      &   (0.200)                  & -608.5                       & (0.200)         \\
                                  & 1.4                  & -637.50                      &   (0.125)                  & -613.70                      & (0.100)         \\
                                  & 1.6                  & -642.40                      &   (0.118)                  & -618.70                      & (0.105)         \\
                                  & 1.8                  & -647.30                      &   (0.085)                  & -622.60                      & (0.090)         \\
                                  & 2.0                  & -651.00                      &   (0.080)                  & -627.30                      & (0.070)         \\
                                  & 2.5                  & -658.80                      &   (0.040)                  & -634.50                      & (0.050)         \\ \hline
\end{tabular}
\end{table}
\end{frame}

\begin{frame}\frametitle{Resultados - Regra de Política Monetária}
\begin{table}[h!]
  \tiny
  \centering
\caption{Regras da Política Monetária estimadas}
\begin{tabular}{cccc}
\hline
\multicolumn{4}{c}{Estimação 1}                                                  \\ \hline
Parâmetro                    & Priori               & DSGE         & DSGE-VAR    \\ \hline
$\psi_{\pi}$                 & 1.50 (0.50)          & 2.00 (0.60)  & 1.41 (0.38) \\
$\psi_{y}$                   & 0.25 (0.13)          & 0.48 (0.12)  & 0.45 (0.11) \\
$\psi_{\Delta e}$     & 0.25 (0.13)          & 0.49 (0.12)  & 0.47 (0.11) \\
$\rho_{R}$                   & 0.50 (0.20)          & 0.50 (0.08)  & 0.60 (0.06) \\ \hline
Log Veross. Marginal         &                      & -800.53      & -590.61     \\ \hline
\multicolumn{4}{c}{Estimação 2}                                                  \\ \hline
Parâmetro                   & Priori               & DSGE         & DSGE-VAR     \\
$\psi_{\pi}$                & 1.50 (0.50)          & 2.00 (0.59)  & 1.38 (033)   \\
$\psi_{y}$                  & 0.25 (0.13)          & 0.48 (0.12)  & 0.43 (0.10)  \\
$\psi_{\Delta e}$    & 0.01 (0.50)          & 1.18 (11.22) & 0.45 (0.20)  \\
$\rho_{R}$                  & 0.50 (0.20)          & 0.49 (0.08)  & 0.61 (0.07)  \\ \hline
Log Veross. Marginal        & \multicolumn{1}{l}{} & -805.97      & -594.75      \\ \hline
\end{tabular}
\end{table}
\end{frame}

% 
% \begin{frame}\frametitle{Resultados - Determinantes da Inflação}
%   \begin{itemize}
%     \item Choques: termos de troca, produto externo, inflação externa, tecnologia e taxa de juros
%     \item De um ponto de vista qualitativo percebemos diferenças entre as funções para todos os choques;
%     \item As funções de resposta ao impulso verdes possuem maior variância, refletindo maiores desvios-padrão estimados para os choques pelo DSGE-VAR($\hat{\lambda}=0.5$);
%     \item De acordo o modelo DSGE-VAR, os choques que tem maior impacto sobre a inflação são externos (produto e inflação);
%     \item Estes resultados sugerem que a autoridade monetária não tem obtido sucesso em isolar a inflação de distúrbios externos.
%   \end{itemize}
% \end{frame}

%%%
% CONCLUSÃO
%%%

\subsection{Conclusões}

\begin{frame}\frametitle{Conclusões}
  \begin{itemize}
    \item Nossa estimativa para a função de reação da política monetária indica que o BACEN responde aos movimentos na taxa de câmbio;
    \item A análise DSGE-VAR se mostrou útil ddado que consegue prevenir as estimativas VAR das restrições geradas pelo modelo DSGE;
    \item Ambas as estimativas DSGE e DSGE-VAR indicam que choques externos geraram variabilidade na inflação;
    \item Em função da má especificação do modelo DSGE, o DSGE-VAR implica em uma dinâmica da resposta aos choques estruturais distinta das funções de resposta o impulso do modelo DSGE.
  \end{itemize}
\end{frame}

%%%
% REFERENCIAL
%%%

\subsection{Referencial Teórico}

\begin{frame}[allowframebreaks]{Referências}
\bibliography{article}
\end{frame}

\end{document}
