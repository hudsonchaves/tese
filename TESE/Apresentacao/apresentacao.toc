\babel@toc {brazil}{}
\beamer@sectionintoc {1}{Ensaio 1 - Movimentos na taxa de c\IeC {\^a}mbio e pol\IeC {\'\i }tica monet\IeC {\'a}ria: Algumas evid\IeC {\^e}ncias para o Brasil usando modelos DSGE-VAR}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Introdu\IeC {\c c}\IeC {\~a}o/Motiva\IeC {\c c}\IeC {\~a}o}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Metodologia}{6}{0}{1}
\beamer@subsectionintoc {1}{3}{Resultados}{8}{0}{1}
\beamer@subsectionintoc {1}{4}{Conclus\IeC {\~o}es}{11}{0}{1}
\beamer@sectionintoc {2}{Ensaio 2 - Um estudo emp\IeC {\'\i }rico da din\IeC {\^a}mica da correla\IeC {\c c}\IeC {\~a}o do retorno das a\IeC {\c c}\IeC {\~o}es do Brasil}{12}{0}{2}
\beamer@subsectionintoc {2}{1}{Introdu\IeC {\c c}\IeC {\~a}o/Motiva\IeC {\c c}\IeC {\~a}o}{13}{0}{2}
\beamer@subsectionintoc {2}{2}{Metodologia}{15}{0}{2}
\beamer@subsectionintoc {2}{3}{Resultados}{19}{0}{2}
\beamer@subsectionintoc {2}{4}{Conclus\IeC {\~o}es}{24}{0}{2}
\beamer@sectionintoc {3}{Ensaio 3 - Usando Big Data, Machine Learning e Text Mining em Macroeconomia}{25}{0}{3}
\beamer@subsectionintoc {3}{1}{Introdu\IeC {\c c}\IeC {\~a}o/Motiva\IeC {\c c}\IeC {\~a}o}{26}{0}{3}
\beamer@subsectionintoc {3}{2}{Metodologia}{28}{0}{3}
\beamer@subsectionintoc {3}{3}{Resultados}{34}{0}{3}
\beamer@subsectionintoc {3}{4}{Conclus\IeC {\~a}o}{37}{0}{3}
