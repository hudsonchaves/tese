\changetocdepth {4}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {INTRODU\IeC {\c C}\IeC {\~A}O}}{15}{chapter.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Movimentos na taxa de c\IeC {\^a}mbio e pol\IeC {\'\i }tica monet\IeC {\'a}ria: Algumas evid\IeC {\^e}ncias para o Brasil usando modelos DSGE-VAR}}{19}{chapter.2}
\contentsline {section}{\numberline {2.1}INTRODU\IeC {\c C}\IeC {\~A}O}{19}{section.2.1}
\contentsline {section}{\numberline {2.2}REFERENCIAL TE\IeC {\'O}RICO}{21}{section.2.2}
\contentsline {section}{\numberline {2.3}METODOLOGIA}{24}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Modelo Te\IeC {\'o}rico}{25}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Modelos DSGE e DSGE-VAR}{26}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}O algoritmo de Del Negro-Schorfheide}{31}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Dados e distribui\IeC {\c c}\IeC {\~a}o a \emph {priori} dos par\IeC {\^a}metros}{32}{subsection.2.3.4}
\contentsline {section}{\numberline {2.4}RESULTADOS}{33}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Estimativas da regra de pol\IeC {\'\i }tica monet\IeC {\'a}ria}{34}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}O ajuste do modelo DSGE para a pequena economia aberta}{36}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Determinantes da infla\IeC {\c c}\IeC {\~a}o}{38}{subsection.2.4.3}
\contentsline {section}{\numberline {2.5}CONCLUS\IeC {\~A}O}{39}{section.2.5}
\contentsline {section}{\numberline {2.6}REFER\IeC {\^E}NCIAS}{41}{section.2.6}
\contentsline {section}{\numberline {2.7}AP\IeC {\^E}NDICE A - GR\IeC {\'A}FICOS}{45}{section.2.7}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Um estudo emp\IeC {\'\i }rico da din\IeC {\^a}mica da correla\IeC {\c c}\IeC {\~a}o do retorno das a\IeC {\c c}\IeC {\~o}es do Brasil}}{50}{chapter.3}
\contentsline {section}{\numberline {3.1}INTRODU\IeC {\c C}\IeC {\~A}O}{50}{section.3.1}
\contentsline {section}{\numberline {3.2}REFERENCIAL TE\IeC {\'O}RICO}{52}{section.3.2}
\contentsline {section}{\numberline {3.3}METODOLOGIA}{55}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Risco de mercado, risco idiossincr\IeC {\'a}tico e correla\IeC {\c c}\IeC {\~a}o}{56}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Modelos GARCH Multivariados}{58}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Testes de Heterocedasticidade Condicional}{62}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Teoria de matrizes aleat\IeC {\'o}rias e autovalores}{64}{subsection.3.3.4}
\contentsline {subsection}{\numberline {3.3.5}Teste de tend\IeC {\^e}ncia linear}{65}{subsection.3.3.5}
\contentsline {subsection}{\numberline {3.3.6}Dados}{66}{subsection.3.3.6}
\contentsline {section}{\numberline {3.4}RESULTADOS}{67}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Heterocedasticidade Condicional}{68}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Din\IeC {\^a}mica da Correla\IeC {\c c}\IeC {\~a}o Condicional}{69}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}An\IeC {\'a}lise do comportamento da correla\IeC {\c c}\IeC {\~a}o condicional}{69}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Uma medida de intensidade da correla\IeC {\c c}\IeC {\~a}o}{72}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Comportamento da intensidade da correla\IeC {\c c}\IeC {\~a}o condicional}{73}{subsection.3.4.5}
\contentsline {section}{\numberline {3.5}CONCLUS\IeC {\~A}O}{75}{section.3.5}
\contentsline {section}{\numberline {3.6}REFER\IeC {\^E}NCIAS}{77}{section.3.6}
\contentsline {section}{\numberline {3.7}AP\IeC {\^E}NDICE A - TABELAS}{81}{section.3.7}
\contentsline {section}{\numberline {3.8}AP\IeC {\^E}NDICE B - GR\IeC {\'A}FICOS}{84}{section.3.8}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Usando Big Data, Machine Learning e Text Mining em Macroeconomia}}{86}{chapter.4}
\contentsline {section}{\numberline {4.1}INTRODU\IeC {\c C}\IeC {\~A}O}{86}{section.4.1}
\contentsline {section}{\numberline {4.2}REFERENCIAL TE\IeC {\'O}RICO}{88}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Web Scraping, novas medidas de infla\IeC {\c c}\IeC {\~a}o e rigidez nos pre\IeC {\c c}os}{89}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}An\IeC {\'a}lise de Sentimento do Banco Central usando Text Mining}{93}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}METODOLOGIA}{95}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Web Scraping}{95}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}An\IeC {\'a}lise de Sentimento}{97}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Dados}{98}{subsection.4.3.3}
\contentsline {section}{\numberline {4.4}RESULTADOS}{99}{section.4.4}
\contentsline {section}{\numberline {4.5}CONCLUS\IeC {\~A}O}{102}{section.4.5}
\contentsline {section}{\numberline {4.6}REFER\IeC {\^E}NCIAS}{104}{section.4.6}
\contentsline {section}{\numberline {4.7}AP\IeC {\^E}NDICE A - NUVEM DE PALAVRAS}{106}{section.4.7}
\contentsline {section}{\numberline {4.8}AP\IeC {\^E}NDICE B - USO DE MACHINE LEARNING EM ECONOMIA}{107}{section.4.8}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {CONCLUS\IeC {\~A}O}}{110}{chapter.5}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\uppercase {REFER\IeC {\^E}NCIAS}}{113}{section*.46}
\renewcommand *{\cftappendixname }{AP\^ENDICE \space }
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {A}\MakeTextUppercase {C\IeC {\'o}digos e dados}}{115}{appendix.A}
\contentsfinish 
