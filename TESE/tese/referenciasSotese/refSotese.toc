\changetocdepth {4}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {INTRODU\IeC {\c C}\IeC {\~A}O}}{15}{chapter.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Movimentos na taxa de c\IeC {\^a}mbio e pol\IeC {\'\i }tica monet\IeC {\'a}ria: Algumas evid\IeC {\^e}ncias para o Brasil usando modelos DSGE-VAR}}{19}{chapter.2}
\contentsline {section}{\numberline {2.1}INTRODU\IeC {\c C}\IeC {\~A}O}{19}{section.2.1}
\contentsline {section}{\numberline {2.2}REFERENCIAL TE\IeC {\'O}RICO}{19}{section.2.2}
\contentsline {section}{\numberline {2.3}METODOLOGIA}{20}{section.2.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {CONCLUS\IeC {\~A}O}}{22}{chapter.3}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\uppercase {REFER\IeC {\^E}NCIAS}}{113}{section*.9}
\contentsfinish 
