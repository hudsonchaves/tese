TESE
==============

Este repositório tem como objetivo disponibilizar informações (dados, códigos, fontes bibliográficas) da Tese construída para o Programa de Pós-Graduação em Economia da Universidade Federal do Rio Grande do Sul (PPGE/UFRGS).  

Em linhas gerais, o repositório está dividido como segue:

* Na pasta **[TESE](https://bitbucket.org/hudsonchaves/tese/src/70ee05197e59486ceeaad5ad8db0e19186cc9671/TESE/?at=master)** você terá acesso à [apresentação](https://bitbucket.org/hudsonchaves/tese/src/4630c52d203002adb5ff6c00e6a15f419fe765c9/TESE/Apresentacao/?at=master), [Ensaio 1](https://bitbucket.org/hudsonchaves/tese/src/4630c52d203002adb5ff6c00e6a15f419fe765c9/TESE/Ensaio1/?at=master), [Ensaio 2](https://bitbucket.org/hudsonchaves/tese/src/4630c52d203002adb5ff6c00e6a15f419fe765c9/TESE/Ensaio2/?at=master), [Ensaio 3](https://bitbucket.org/hudsonchaves/tese/src/4630c52d203002adb5ff6c00e6a15f419fe765c9/TESE/Ensaio3/?at=master) e a versão completa da [tese](https://bitbucket.org/hudsonchaves/tese/src/70ee05197e59486ceeaad5ad8db0e19186cc9671/TESE/tese/?at=master).
* Em cada um dos Ensaios, temos a seguinte estrutura de pastas: 
    * **Artigos**: códigos em LaTex dos artigos bem como os dados e códigos em R e/ou GAUSS de cada artigo. 
	* **Referências**: toda e qualquer referência bibliográfica (LaTex, R, DSGE, Papers Internacionais e Nacionais) importantes para os artigos
	
Além disso, na [wiki](https://bitbucket.org/hudsonchaves/tese/wiki/Home) você encontra detalhes de como replicar os artigos criados aqui. Para tanto, ensinamos como fazer uso do `Ansible`, `Amazon`, `Vagrant`, `VirtualBox`, `Docker` e `Docker-compose` para criar automaticamente o ambiente necessário para fazer as análises seja localmente (seu computador) ou em servidor da Amazon. 

---

**Nota**: Qualquer material disponibilizado aqui pode ser utilizado desde que referenciado. Caso você encontre algum problema ou dúvida com os códigos fique à vontade para postar uma pergunta neste [link](https://bitbucket.org/hudsonchaves/tese/issues?status=new&status=open). O quanto antes tentarei solucionar e deixar comentários para que outras pessoas também tenham acesso.
**lembrando que os serviços usados na Amazon geram custos e você deve ter conhecimento de como usá-los**
---